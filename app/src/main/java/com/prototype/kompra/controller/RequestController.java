package com.prototype.kompra.controller;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.activity.ItemRequestActivity;
import com.prototype.kompra.api.RequestDetailApi;
import com.prototype.kompra.fragment.Request;
import com.prototype.kompra.model.RequestDetail;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RequestController implements Callback<RequestDetail> {

    AppCompatActivity appCompatActivity;
    Gson gson;
    Retrofit retrofit;
    Request request;

    public RequestController(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    public RequestController(Request request) {
        this.request = request;
    }

    public void start() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
         gson = new GsonBuilder()
                .setLenient()
                .create();

        Context context;

        if (appCompatActivity != null) {
            context = appCompatActivity;
        } else {
            context = request.getActivity();
        }

        retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(context).build())
                .build();
    }

    @Override
    public void onResponse(Call<RequestDetail> call, Response<RequestDetail> response) {

        if(response.isSuccessful()) {
            RequestDetail requestDetail = response.body();
            if (appCompatActivity !=null) {
                ((ItemRequestActivity) appCompatActivity).response(requestDetail.getData(), response.message(), requestDetail.getStatusCode());
            }
            else if (request !=null) {
                request.response(requestDetail.getData(), response.message(), requestDetail.getStatusCode());
            }
        } else {
            System.out.println(response.errorBody());
        }
    }

    public void displayRequest(Integer id) {
        RequestDetailApi requestDetailApi = retrofit.create(RequestDetailApi.class);

        Call<RequestDetail> call = requestDetailApi.getRequestDetailBuyer(id);
        call.enqueue(this);
    }

    @Override
    public void onFailure(Call<RequestDetail> call, Throwable t) {
        if (appCompatActivity !=null) {
            ((ItemRequestActivity) appCompatActivity).fail(t);
        } else {
            request.fail(t);
        }
    }
}
