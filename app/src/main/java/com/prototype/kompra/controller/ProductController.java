package com.prototype.kompra.controller;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.api.ProductApi;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProductController implements Callback<Product> {

    Context context;
    public ProductController() {

    }

    public void start() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(context).build())
                .build();

        ProductApi productApi = retrofit.create(ProductApi.class);

        Call<Product> call = productApi.getAllProduct();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Product> call, Response<Product> response) {

        if(response.isSuccessful()) {
            Product resultResponse = response.body();

        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<Product> call, Throwable t) {

    }
}
