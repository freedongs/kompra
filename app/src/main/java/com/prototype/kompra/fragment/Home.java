package com.prototype.kompra.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prototype.kompra.R;
import com.prototype.kompra.adapter.CategoryAdapter;
import com.prototype.kompra.controller.CategoryContoller;
import com.prototype.kompra.model.Category;
import com.prototype.kompra.util.CallbackResponseTemplate;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;

public class Home extends Fragment implements CallbackResponseTemplate {

    CategoryContoller categoryContoller;
    RecyclerView recyclerView;
    CategoryAdapter categoryAdapter;

    @Override
    public void onResume() {
        super.onResume();
        categoryContoller = new CategoryContoller(this);
        try {
            categoryContoller.start();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = view.findViewById(R.id.category_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
    }

    @Override
    public void response(List result, String message, Integer statusCode) {
        recyclerView.setAdapter(null);
        categoryAdapter = new CategoryAdapter((List<Category.Datum>) result);
        recyclerView.setAdapter(categoryAdapter);
    }

    @Override
    public void fail(Throwable stackTrace) {
        System.out.println(stackTrace.getMessage());
    }
}
