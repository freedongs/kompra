package com.prototype.kompra.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.prototype.kompra.R;
import com.prototype.kompra.adapter.OrderDetailProductsAdapter;
import com.prototype.kompra.controller.ResponseUpdateController;
import com.prototype.kompra.fragment.Home;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.util.CallbackTemplateSingleObject;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class OrderActivity extends AppCompatActivity implements CallbackTemplateSingleObject {

    OrderMasterlist.Datum datum;
    TextView order_number, order_total, payment_type, payment_date, address, status;
    LinearLayout background, layout_runner, layout_feedback;
    RecyclerView order_products_recycler_view;
    OrderDetailProductsAdapter orderDetailProductsAdapter;
    Button feedback, cancel_order;
    TextView runner_name, runner_contact_number, license_number, rate_feedback;
    RatingBar ratingBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_processing);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        order_number = findViewById(R.id.order_number);
        order_total = findViewById(R.id.order_total);
        payment_type = findViewById(R.id.payment_type);
        payment_date = findViewById(R.id.payment_date);
        address = findViewById(R.id.address);
        status = findViewById(R.id.status);
        background = findViewById(R.id.status_linear);
        layout_runner = findViewById(R.id.layout_runner);
        layout_feedback = findViewById(R.id.layout_feedback);
        feedback = findViewById(R.id.feedback);
        cancel_order = findViewById(R.id.cancel_order);
        runner_name = findViewById(R.id.runner_name);
        runner_contact_number = findViewById(R.id.runner_contact_number);
        license_number = findViewById(R.id.license_number);
        rate_feedback = findViewById(R.id.rate_feedback);
        ratingBar = findViewById(R.id.rating);
        order_products_recycler_view = findViewById(R.id.order_products_recycler_view);
        order_products_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        order_products_recycler_view.setItemAnimator(new DefaultItemAnimator());
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderActivity.this, FeedbackActivity.class);
                intent.putExtra("feedback", datum);
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("container");
        datum = (OrderMasterlist.Datum) args.getSerializable("order");

        getSupportActionBar().setTitle("Order details");

        if(datum.getEMPID() != null) {
            layout_runner.setVisibility(View.VISIBLE);
            runner_name.setText(datum.getEmployee().getEMPFname() + " " + datum.getEmployee().getEMPLname());
            runner_contact_number.setText(datum.getEmployee().getEMPConNum());
            license_number.setText(datum.getEmployee().getDriverDetail().getDriverLicenseNum());
        }

        if(datum.getFEEDRATEID() != null) {
            layout_feedback.setVisibility(View.VISIBLE);
            ratingBar.setRating(Float.parseFloat(datum.getFeedbackRating().getORDERRATE().toString()));
            rate_feedback.setText(datum.getFeedbackRating().getORDERFEEDBACKS());
        }

        orderDetailProductsAdapter = new OrderDetailProductsAdapter(datum.getOrderDetails(), datum);
        order_products_recycler_view.setAdapter(orderDetailProductsAdapter);
        order_number.setText(datum.getOrMasID().toString());
        order_total.setText(datum.getTotAmount().toString());
        payment_type.setText(datum.getPaymentType());
        payment_date.setText(datum.getDeliveryDate());
        address.setText(datum.getShippingAdd());
        if (datum.getOrTracID() == null) {
            cancel_order.setVisibility(View.VISIBLE);
            status.setText("Waiting for a runner...");
        } else if(datum.getOrTracID() == 1){
            cancel_order.setVisibility(View.VISIBLE);
            background.setBackgroundResource(R.drawable.background_pending);
            status.setText("Pending..");
        } else if(datum.getOrTracID() == 2){
            cancel_order.setVisibility(View.VISIBLE);
            status.setText("Processing");
            background.setBackgroundResource(R.drawable.background_processing);
        } else if(datum.getOrTracID() == 3){
            status.setText("On the way...");
            background.setBackgroundResource(R.drawable.background_on_d_way);
        } else if(datum.getOrTracID() == 4){
            status.setText("Received.");
            background.setBackgroundResource(R.drawable.background_received);
            if(datum.getFEEDRATEID() == null) {
                feedback.setVisibility(View.VISIBLE);
            } else {
                feedback.setVisibility(View.GONE);
            }
        } else if (datum.getOrTracID() == 5){
            status.setText("Cancelled.");
        }

        cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datum.setOrTracID(5);
                ResponseUpdateController responseUpdateController = new ResponseUpdateController(OrderActivity.this);
                try {
                    responseUpdateController.start();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                responseUpdateController.update(datum);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(OrderActivity.this, HomePageActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
        return true;
    }

    @Override
    public void response(Object result, String message, Integer statusCode) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(OrderActivity.this);
        builder1.setMessage("Order is cancelled!");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

        alert11.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Intent i = new Intent(OrderActivity.this, HomePageActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }

    @Override
    public void fail(Throwable stackTrace) {
        System.out.println(stackTrace.getMessage());
    }
}
