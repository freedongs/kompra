package com.prototype.kompra.api;

import com.prototype.kompra.model.DriverDetail;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DriverDetailApi {

    @GET("driverdetail")
    Call<DriverDetail> getAllDriverDetail();

    @GET("driverdetail/{id}")
    Call<DriverDetail> getDriverDetail(@Path("id") int id);

    @POST("driverdetail")
    Call<DriverDetail> addDriverDetail(@Body DriverDetail driverDetail);

    @POST("driverdetail-delete/{id}")
    Call<DriverDetail> removeDriverDetail(@Path("id") int id);

    @POST("driverdetail/{id}")
    Call<DriverDetail> updateDriverDetail(@Path("id") int id, @Body DriverDetail driverDetail);
}
