package com.prototype.kompra.api;

import com.prototype.kompra.model.Return;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ReturnApi {

    @GET("returnitems")
    Call<Return> getAllReturn();

    @GET("returnitems/{id}")
    Call<Return> getReturn(@Path("id") int id);

    @POST("returnitems")
    Call<Return> addReturn(@Body Return.Datum r);

    @POST("returnitems-delete/{id}")
    Call<Return> removeReturn(@Path("id") int id);

    @POST("returnitems/{id}")
    Call<Return> updateReturn(@Path("id") int id, @Body Return r);
}
