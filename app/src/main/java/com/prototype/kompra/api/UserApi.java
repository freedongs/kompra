package com.prototype.kompra.api;

import com.prototype.kompra.model.Update;
import com.prototype.kompra.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UserApi {

    @GET("users")
    Call<User> getAllUser();

    @GET("users/{id}")
    Call<User> getUser(@Path("id") int id);

    @POST("users")
    Call<User> addUser(@Body User user);

    @POST("users-delete/{id}")
    Call<User> removeUser(@Path("id") int id);

    @POST("users/{id}")
    Call<Update> updateUser(@Path("id") int id, @Body User.Data user);

    @POST("login")
    Call<User> login(@Body User.Data user);

    @POST("register")
    Call<User> register(@Body User.Data user);
}
