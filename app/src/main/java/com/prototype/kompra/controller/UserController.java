package com.prototype.kompra.controller;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.R;
import com.prototype.kompra.activity.LoginActivity;
import com.prototype.kompra.activity.RegistrationActivity;
import com.prototype.kompra.activity.RunnerLoginActivity;
import com.prototype.kompra.api.UserApi;
import com.prototype.kompra.model.User;
import com.prototype.kompra.util.App;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserController implements Callback<User> {

    private AppCompatActivity appCompatActivity;
    UserApi userApi;
    Retrofit retrofit;
    Gson gson;

    public UserController(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    public void start() throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {

        gson = new GsonBuilder()
                .setLenient()
                .create();

        // Load CAs from an InputStream
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");

        InputStream inputStream = appCompatActivity.getResources().openRawResource(R.raw.my_cert); //(.crt)
        Certificate certificate = certificateFactory.generateCertificate(inputStream);
        inputStream.close();

        // Create a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", certificate);

        // Create a TrustManager that trusts the CAs in our KeyStore.
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm);
        trustManagerFactory.init(keyStore);

        TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
        X509TrustManager x509TrustManager = (X509TrustManager) trustManagers[0];


        // Create an SSLSocketFactory that uses our TrustManager
        SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, new TrustManager[]{x509TrustManager}, null);


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .sslSocketFactory(sslContext.getSocketFactory(), x509TrustManager)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        userApi = retrofit.create(UserApi.class);

    }

    public void register(User.Data data) {

        Call<User> call = userApi.register(data);
        call.enqueue(this);
    }

    public void login(User.Data data){

        Call<User> call = userApi.login(data);
        call.enqueue(this);
    }


    @Override
    public void onResponse(Call<User> call, Response<User> response) {

        if(response.isSuccessful()) {
            User user = response.body();
            if (appCompatActivity instanceof RegistrationActivity) {
                ((RegistrationActivity) appCompatActivity).response(user.getData(), response.message(), user.getStatusCode());
            } else if (appCompatActivity instanceof  LoginActivity) {
                ((LoginActivity) appCompatActivity).response(user, response.message(), user.getStatusCode());
            } else {
                ((RunnerLoginActivity) appCompatActivity).response(user, response.message(), user.getStatusCode());
            }
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {
        if (appCompatActivity instanceof RegistrationActivity) {
            ((RegistrationActivity) appCompatActivity).fail(t);
        } else if (appCompatActivity instanceof  LoginActivity){
            ((LoginActivity) appCompatActivity).fail(t);
        } else {
            ((RunnerLoginActivity) appCompatActivity).fail(t);
        }
    }
}
