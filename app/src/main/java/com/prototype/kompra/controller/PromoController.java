package com.prototype.kompra.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.api.CategoryApi;
import com.prototype.kompra.api.PromoApi;
import com.prototype.kompra.fragment.Cart;
import com.prototype.kompra.model.Category;
import com.prototype.kompra.model.Promo;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PromoController implements Callback<Promo> {

    Cart cart;

    public PromoController(Cart cart) {
        this.cart = cart;
    }

    public void start(String code) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(cart.getActivity()).build())
                .build();

        PromoApi promoApi = retrofit.create(PromoApi.class);

        Call<Promo> call = promoApi.getPromo(code);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Promo> call, Response<Promo> response) {

        if(response.isSuccessful()) {
            Promo resultResponse = response.body();
            cart.response(resultResponse.getData(), response.message(),resultResponse.getStatusCode());
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<Promo> call, Throwable t) {

        cart.fail(t);
    }
}
