package com.prototype.kompra.api;

import com.prototype.kompra.model.Product;
import com.prototype.kompra.model.Search;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SearchApi {

    @POST("search")
    Call<Product> search(@Body Search search);
}
