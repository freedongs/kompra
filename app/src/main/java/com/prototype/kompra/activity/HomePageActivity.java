package com.prototype.kompra.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.prototype.kompra.R;
import com.prototype.kompra.fragment.Base;
import com.prototype.kompra.fragment.Fave;
import com.prototype.kompra.fragment.Profile;
import com.prototype.kompra.fragment.Request;
import com.prototype.kompra.model.User;
import com.prototype.kompra.util.Session;


public class HomePageActivity extends AppCompatActivity implements MenuItem.OnActionExpandListener{

    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Base()).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();

        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        if(menuItem.isChecked())
                            menuItem.setChecked(false);
                        else
                            menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        switch (menuItem.getItemId()) {

                            case R.id.nav_home:
                                getSupportActionBar().setTitle("Home");
                                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Base()).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();
                                return true;
                            case R.id.nav_profile:
                                getSupportActionBar().setTitle("Profile");
                                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Profile()).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();
                                return true;
                            case R.id.nav_request:
                                getSupportActionBar().setTitle("Request");
                                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Request()).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();
                                return true;
                            case R.id.nav_favorite:
                                getSupportActionBar().setTitle("Favorite");
                                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Fave()).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();
                                return true;
                            case R.id.nav_signout:
                                Session session = new Session(HomePageActivity.this);
                                session.clearSession(getString(R.string.user));
                                session.clearSession(getString(R.string.cart));
                                session.clearSession(getString(R.string.token));
                                session.clearSession(getString(R.string.id));
                                Intent i = new Intent(HomePageActivity.this, LoginActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                finish();
                                return true;
                            default:
                                Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                                return true;

                        }
                    }
                });

        View header = navigationView.getHeaderView(0);
        final TextView userName, userEmail;
        userName = (TextView) header.findViewById(R.id.username);
        userEmail = (TextView) header.findViewById(R.id.email);

        Session session = new Session(this);
        User data = session.getUser(this.getString(R.string.user));
        userEmail.setText(data.getData().getEmail());
        userName.setText(data.getData().getBuyer().getBuyLname() + ", " + data.getData().getBuyer().getBuyFname());


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout, R.string.openDrawer, R.string.closeDrawer){
            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_order, menu);
        menu.findItem(R.id.menu_search).setOnActionExpandListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_request) {
            startActivity(new Intent(this, ItemRequestActivity.class));
            return true;
        } else if (id == android.R.id.home) {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawers();
            } else {
                mDrawerLayout.openDrawer(Gravity.START);
            }
            return true;
        } else if (id == R.id.menu_search) {
            onSearchRequested();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSearchRequested() {
        Bundle appData = new Bundle();
        appData.putBoolean(SearchableActivity.IS_CATEGORY, true);
        startSearch(null, false, appData, false);
        return true;
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        getSupportActionBar().setIcon(R.drawable.search);
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        return false;
    }
}
