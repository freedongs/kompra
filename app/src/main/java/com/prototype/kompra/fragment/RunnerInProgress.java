package com.prototype.kompra.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prototype.kompra.R;
import com.prototype.kompra.adapter.RunnerInProgressAdapter;
import com.prototype.kompra.controller.OrderController;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.util.CallbackResponseTemplate;
import com.prototype.kompra.util.Session;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;

public class RunnerInProgress extends Fragment implements CallbackResponseTemplate {

    RecyclerView runner_in_progress;
    RunnerInProgressAdapter runnerInProgressAdapter;
    OrderController orderController;
    Session session;

    @Override
    public void onResume() {
        super.onResume();
        orderController = new OrderController(this);
        try {
            orderController.start();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        session = new Session(getActivity());
        orderController.getEmployeeProgress(session.getId(getActivity().getString(R.string.id)));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_in_progress, container, false);
        runner_in_progress = view.findViewById(R.id.runner_in_progress);
        runner_in_progress.setLayoutManager(new LinearLayoutManager(getActivity()));
        runner_in_progress.setItemAnimator(new DefaultItemAnimator());
        return  view;
    }

    @Override
    public void response(List result, String message, Integer statusCode) {
        runner_in_progress.setAdapter(null);
        runnerInProgressAdapter = new RunnerInProgressAdapter((List<OrderMasterlist.Datum>) result);
        runner_in_progress.setAdapter(runnerInProgressAdapter);
    }

    @Override
    public void fail(Throwable stackTrace) {

    }
}
