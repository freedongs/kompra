package com.prototype.kompra.controller;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.adapter.ProductAdapter;
import com.prototype.kompra.api.FavoriteApi;
import com.prototype.kompra.api.OrderMasterlistApi;
import com.prototype.kompra.fragment.Fave;
import com.prototype.kompra.model.CustomFavorite;
import com.prototype.kompra.model.CustomOrderMasterlist;
import com.prototype.kompra.model.Favorite;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CustomFavoriteController implements Callback<CustomFavorite> {

    ProductAdapter productAdapter;
    Context context;
    Gson gson;
    Retrofit retrofit;

    public CustomFavoriteController(ProductAdapter productAdapter, Context context) {
        this.productAdapter = productAdapter;
        this.context = context;
    }

    public void start(Favorite.Data2 data) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(context).build())
                .build();

        FavoriteApi favoriteApi = retrofit.create(FavoriteApi.class);

        Call<CustomFavorite> call = favoriteApi.addFavorite(data);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<CustomFavorite> call, Response<CustomFavorite> response) {
        if(response.isSuccessful()) {
            CustomFavorite favorite = response.body();
            productAdapter.response(favorite.getData(), response.message(), favorite.getStatusCode());
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<CustomFavorite> call, Throwable t) {
        productAdapter.fail(t);
    }
}
