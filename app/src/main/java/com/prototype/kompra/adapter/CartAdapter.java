package com.prototype.kompra.adapter;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.prototype.kompra.R;
import com.prototype.kompra.activity.HomePageActivity;
import com.prototype.kompra.activity.LoginActivity;
import com.prototype.kompra.activity.RegistrationActivity;
import com.prototype.kompra.fragment.Cart;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.util.App;
import com.prototype.kompra.util.Session;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ProductViewHolder>{

    List<Product.Datum> list;
    Cart cart;

    public CartAdapter(List<Product.Datum> list, Cart cart)
    {
        this.list = list;
        this.cart = cart;
    }

    @NonNull
    @Override
    public CartAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cart_recycler_view, viewGroup, false);
        return new CartAdapter.ProductViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    @Override
    public void onBindViewHolder(@NonNull CartAdapter.ProductViewHolder productViewHolder, int i) {
        final Product.Datum datum = list.get(i);
        productViewHolder.productName.setText(datum.getProdName());
        productViewHolder.productDescription.setText(datum.getProdDesc());
        productViewHolder.productPrize.setText(String.format("PHP %.2f", datum.getProdPrice()));
        productViewHolder.productQuantity.setText(datum.getProdQuantity().toString());
        productViewHolder.product_category.setText(datum.getCategory().getCategoryName());
//        if(datum.getProdPhoto() != null)
//            Picasso.get().load(App.image_url + datum.getFile().getImageUrl()).placeholder(R.drawable.loading_animation).fit().into(productViewHolder.productPic);
//        else
//            productViewHolder.productPic.setImageResource(R.drawable.default_product);
        productViewHolder.datum = datum;
        productViewHolder.remove_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(v.getContext());
                builder1.setMessage("Product has been removed from the cart.");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

                alert11.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Session session = new Session(v.getContext());
                        list.remove(datum);
                        session.addCart(v.getContext().getString(R.string.cart), list);
                        notifyDataSetChanged();
                        cart.checkIfCartEmpty(list);
                    }
                });
            }
        });
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView productPic;
        public TextView productName;
        public TextView productDescription;
        public TextView productPrize;
        public TextView productQuantity;
        public TextView product_category;
        public Product.Datum datum;
        public Button remove_product;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);

            productPic = (ImageView) itemView.findViewById(R.id.product_image);
            productName = (TextView) itemView.findViewById(R.id.product_name);
            productDescription = (TextView) itemView.findViewById(R.id.product_description);
            productPrize = (TextView) itemView.findViewById(R.id.product_prize);
            productQuantity = (TextView) itemView.findViewById(R.id.product_quantity);
            product_category = (TextView) itemView.findViewById(R.id.product_category);
            remove_product = itemView.findViewById(R.id.remove_product);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final Dialog dialogbase = new Dialog(v.getContext());
            dialogbase.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogbase.setCanceledOnTouchOutside(true);

            View view = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_add_product, null);

            TextView productName = view.findViewById(R.id.product_name);
            TextView productDesc = view.findViewById(R.id.product_description);
            TextView productPrize = view.findViewById(R.id.product_prize);
            TextView product_category = view.findViewById(R.id.product_category);

            final EditText quantity = view.findViewById(R.id.product_quantity);
            Button add = view.findViewById(R.id.add_product);

            productName.setText(datum.getProdName());
            productDesc.setText(datum.getProdDesc());
            productPrize.setText(datum.getProdPrice().toString());
            quantity.setText(datum.getProdQuantity().toString());
            product_category.setText(datum.getCategory().getCategoryName());
            add.setText("Edit");

            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    if(quantity.getText().toString().trim().length() == 0){
                        quantity.setError("Field must not be empty");
                        return;
                    }

                    Session session = new Session(v.getContext());
                    Product.Datum temp = list.get(list.indexOf(datum));
                    temp.setProdQuantity(Integer.parseInt(quantity.getText().toString()));
                    list.set(list.indexOf(datum), temp);
                    session.addCart(v.getContext().getString(R.string.cart), list);

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(v.getContext());
                    builder1.setMessage("Product quantity is been edited");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                    alert11.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            notifyDataSetChanged();
                        }
                    });
                }
            });
            dialogbase.setContentView(view);
            dialogbase.show();
        }
    }
}
