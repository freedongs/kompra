package com.prototype.kompra.api;

import com.prototype.kompra.model.FeedbackAndRate;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface FeedbackAndRateApi {

    @GET("feedbackandrates")
    Call<FeedbackAndRate> getAllFeedbackAndRate();

    @GET("feedbackandrates/{id}")
    Call<FeedbackAndRate> getFeedbackAndRate(@Path("id") int id);

    @POST("feedbackandrates")
    Call<FeedbackAndRate> addFeedbackAndRate(@Body FeedbackAndRate.Data feedbackAndRate);

    @POST("feedbackandrates-delete/{id}")
    Call<FeedbackAndRate> removeFeedbackAndRate(@Path("id") int id);

    @POST("feedbackandrates/{id}")
    Call<FeedbackAndRate> updateFeedbackAndRate(@Path("id") int id, @Body FeedbackAndRate feedbackAndRate);
}
