package com.prototype.kompra.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prototype.kompra.R;
import com.prototype.kompra.activity.UpdateUserActivity;
import com.prototype.kompra.model.User;
import com.prototype.kompra.util.CallbackTemplateSingleObject;
import com.prototype.kompra.util.Session;

public class Profile extends Fragment {

    TextView firstName, lastName, address, occupation, gender, age, dob, email, password, contact_number, driver_lens, senior_citizen;
    LinearLayout driver_lens_layout;
    Button edit;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        firstName = view.findViewById(R.id.firstName);
        lastName = view.findViewById(R.id.lastName);
        address = view.findViewById(R.id.address);
        occupation = view.findViewById(R.id.occupation);
        gender = view.findViewById(R.id.gender);
        age = view.findViewById(R.id.age);
        dob = view.findViewById(R.id.dob);
        contact_number = view.findViewById(R.id.contact_number);
        driver_lens = view.findViewById(R.id.driver_lens);
        driver_lens_layout = view.findViewById(R.id.driver_lens_layout);
        senior_citizen = view.findViewById(R.id.senior_citizen);
        email = view.findViewById(R.id.email);
        password = view.findViewById(R.id.password);
        edit = (Button) view.findViewById(R.id.edit);

        Session session = new Session(getActivity());

        User data = session.getUser(this.getString(R.string.user));
        if(data.getData().getUserType().equals("buyer")){
            firstName.setText(data.getData().getBuyer().getBuyFname());
            lastName.setText(data.getData().getBuyer().getBuyLname());
            address.setText(data.getData().getBuyer().getBuyAdd());
            occupation.setText(data.getData().getBuyer().getBuyOccu());
            gender.setText(data.getData().getBuyer().getBuyOccu());
            age.setText(data.getData().getBuyer().getBuyAge().toString());
            dob.setText(data.getData().getBuyer().getBuyDoB());
            contact_number.setText(data.getData().getBuyer().getBuyConNum());
            email.setText(data.getData().getEmail());
            password.setText(data.getData().getPassword());
            senior_citizen.setText(data.getData().getSeniorCitizenID());
        } else {
            edit.setVisibility(View.GONE);
            driver_lens_layout.setVisibility(View.VISIBLE);
            firstName.setText(data.getData().getEmployee().getEMPFname());
            lastName.setText(data.getData().getEmployee().getEMPLname());
            address.setText(data.getData().getEmployee().getEMPAdd());
            occupation.setText(data.getData().getEmployee().getEMPPosi());
            gender.setText("Undefined");
            age.setText(data.getData().getEmployee().getEMPAge().toString());
            dob.setText(data.getData().getEmployee().getEMPDoB());
            contact_number.setText(data.getData().getEmployee().getEMPConNum());
            email.setText(data.getData().getEmail());
            password.setText(data.getData().getPassword());
            driver_lens.setText(data.getData().getEmployee().getDriverDetail().getDriverLicenseNum());
        }


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), UpdateUserActivity.class));
            }
        });

        return view;
    }
}
