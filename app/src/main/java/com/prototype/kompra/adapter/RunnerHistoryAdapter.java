package com.prototype.kompra.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prototype.kompra.R;
import com.prototype.kompra.activity.InProgressActivity;
import com.prototype.kompra.activity.ReceivedActivity;
import com.prototype.kompra.model.OrderMasterlist;

import java.util.List;

public class RunnerHistoryAdapter extends RecyclerView.Adapter<RunnerHistoryAdapter.OrderViewHolder>{

    List<OrderMasterlist.Datum> orderMasterlistList;

    public RunnerHistoryAdapter(List<OrderMasterlist.Datum> orderMasterlistList) {
        this.orderMasterlistList = orderMasterlistList;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new OrderViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_runner_order_recycler_view, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder orderViewHolder, int i) {
        OrderMasterlist.Datum datum = orderMasterlistList.get(i);
        orderViewHolder.order_number.setText(datum.getOrMasID().toString());
        orderViewHolder.order_total.setText(datum.getTotAmount().toString());
        orderViewHolder.payment_type.setText(datum.getPaymentType());
        orderViewHolder.payment_date.setText(datum.getDeliveryDate());
        orderViewHolder.address.setText(datum.getShippingAdd());
        orderViewHolder.datum = datum;
    }

    @Override
    public int getItemCount() {
        return orderMasterlistList.size();
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder{

        public TextView order_number, order_total, payment_type, payment_date, address, status;
        public LinearLayout background;
        public OrderMasterlist.Datum datum;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);
            order_number = itemView.findViewById(R.id.order_number);
            order_total = itemView.findViewById(R.id.order_total);
            payment_type = itemView.findViewById(R.id.payment_type);
            payment_date = itemView.findViewById(R.id.payment_date);
            address = itemView.findViewById(R.id.address);
            status = itemView.findViewById(R.id.status);
            background = itemView.findViewById(R.id.status_linear);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(v.getContext(), ReceivedActivity.class);
                    Bundle args = new Bundle();
                    args.putSerializable("order", datum);
                    intent.putExtra("container", args);
                    v.getContext().startActivity(intent);
                }
            });
        }
    }
}
