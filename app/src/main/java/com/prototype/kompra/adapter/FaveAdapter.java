package com.prototype.kompra.adapter;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.prototype.kompra.R;
import com.prototype.kompra.controller.ResponseUpdateController;
import com.prototype.kompra.model.Favorite;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.util.App;
import com.prototype.kompra.util.CallbackTemplateSingleObject;
import com.prototype.kompra.util.Session;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

public class FaveAdapter extends RecyclerView.Adapter<FaveAdapter.ProductViewHolder> implements CallbackTemplateSingleObject {

    List<Favorite.Data2> data2List;

    public FaveAdapter(List<Favorite.Data2> data2List) {
        this.data2List = data2List;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ProductViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_fave_recycler_view, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder productViewHolder, int i) {
        final Favorite.Data2 data2 = data2List.get(i);
        productViewHolder.productName.setText(data2.getProduct().getProdName());
        if(data2.getProduct().getProdPhoto() != null)
            Picasso.get().load(App.image_url +data2.getProduct().getFile().getImageUrl()).placeholder(R.drawable.loading_animation).fit().into(productViewHolder.productPic);
        else
            productViewHolder.productPic.setImageResource(R.drawable.default_product);
        productViewHolder.datum = data2.getProduct();
        productViewHolder.un_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(v.getContext());
                builder1.setMessage("Remove product from favorite!");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface notify, int id) {
                                ResponseUpdateController responseUpdateController = new ResponseUpdateController(FaveAdapter.this, v.getContext());
                                try {
                                    responseUpdateController.start2();
                                } catch (CertificateException e) {
                                    e.printStackTrace();
                                } catch (NoSuchAlgorithmException e) {
                                    e.printStackTrace();
                                } catch (KeyStoreException e) {
                                    e.printStackTrace();
                                } catch (KeyManagementException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                responseUpdateController.delete(data2.getId());
                                data2List.remove(data2);
                                notifyDataSetChanged();
                                notify.cancel();
                                Toast.makeText(v.getContext(), "Product is removed as favorite", Toast.LENGTH_LONG).show();
                            }
                        });

                builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data2List.size();
    }

    @Override
    public void response(Object result, String message, Integer statusCode) {

    }

    @Override
    public void fail(Throwable stackTrace) {

    }


    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView productName;
        public ImageView productPic, un_favorite;
        public Product.Datum datum;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            productPic = (ImageView) itemView.findViewById(R.id.product_image);
            un_favorite = (ImageView) itemView.findViewById(R.id.un_favorite);
            productName =(TextView) itemView.findViewById(R.id.product_name);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final Dialog dialog = new Dialog(v.getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(true);

            View view = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_add_product, null);

            TextView productName = view.findViewById(R.id.product_name);
            TextView productDesc = view.findViewById(R.id.product_description);
            TextView productPrize = view.findViewById(R.id.product_prize);
            final EditText quantity = view.findViewById(R.id.product_quantity);
            ImageView productPic = view.findViewById(R.id.image);
            Button add = view.findViewById(R.id.add_product);

            productName.setText(datum.getProdName());
            productDesc.setText(datum.getProdDesc());
            productPrize.setText(datum.getProdPrice().toString());
            if(datum.getProdPhoto() != null)
                Picasso.get().load(App.image_url +datum.getFile().getImageUrl()).placeholder(R.drawable.loading_animation).fit().into(productPic);
            else
                productPic.setImageResource(R.drawable.default_product);

            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(quantity.getText().toString().trim().length() == 0){
                        quantity.setError("Field must not be empty");
                        return;
                    }

                    Session session = new Session(v.getContext());
                    ArrayList<Product.Datum> data;

                    if (session.getSharedPreferences().contains(v.getContext().getString(R.string.cart))) {
                        data = new ArrayList<>(session.getProducts(v.getContext().getString(R.string.cart)));
                    } else {
                        data = new ArrayList<>();
                    }

                    Product.Datum temp = datum;
                    temp.setProdQuantity(Integer.parseInt(quantity.getText().toString()));
                    data.add(temp);
                    session.addCart(v.getContext().getString(R.string.cart), data);

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(v.getContext());
                    builder1.setMessage("Product added to the cart!");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface notify, int id) {
                                    notify.cancel();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            });

            dialog.setContentView(view);

            dialog.show();
        }
    }
}
