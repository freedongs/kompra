package com.prototype.kompra.controller;

import com.prototype.kompra.activity.PaymentAndShippingActvity;
import com.prototype.kompra.model.OrderDetail;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailController implements Callback<OrderDetail> {

    PaymentAndShippingActvity paymentAndShippingActvity;

    public OrderDetailController(PaymentAndShippingActvity paymentAndShippingActvity) {
        this.paymentAndShippingActvity = paymentAndShippingActvity;
    }

    @Override
    public void onResponse(Call<OrderDetail> call, Response<OrderDetail> response) {
        if(response.isSuccessful()) {
            OrderDetail orderDetail = response.body();
            paymentAndShippingActvity.response(orderDetail.getData(), response.message(), orderDetail.getStatusCode());
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<OrderDetail> call, Throwable t) {

        paymentAndShippingActvity.fail(t);
    }
}
