package com.prototype.kompra.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.prototype.kompra.R;
import com.prototype.kompra.activity.PaymentAndShippingActvity;
import com.prototype.kompra.adapter.CartAdapter;
import com.prototype.kompra.adapter.OrderAdapter;
import com.prototype.kompra.controller.OrderController;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.util.CallbackResponseTemplate;
import com.prototype.kompra.util.Session;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;

public class Transaction extends Fragment implements CallbackResponseTemplate {

    RecyclerView recyclerView;
    OrderAdapter orderAdapter;
    OrderController orderController;

    @Override
    public void onResume() {
        super.onResume();
        orderController = new OrderController(this);
        try {
            orderController.start();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Session session = new Session(getActivity());
        orderController.getOrders(session.getId(getActivity().getString(R.string.id)));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaction, container, false);
        recyclerView = view.findViewById(R.id.order_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
    }

    @Override
    public void response(List result, String message, Integer statusCode) {
        recyclerView.setAdapter(null);
        orderAdapter = new OrderAdapter((List<OrderMasterlist.Datum>) result);
        recyclerView.setAdapter(orderAdapter);
    }

    @Override
    public void fail(Throwable stackTrace) {
        System.out.println(stackTrace.getMessage());
    }
}
