package com.prototype.kompra.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.api.FavoriteApi;
import com.prototype.kompra.fragment.Fave;
import com.prototype.kompra.model.Favorite;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FavoriteController implements Callback<Favorite> {

    Fave fave;
    Gson gson;
    Retrofit retrofit;

    public FavoriteController(Fave fave) {
        this.fave = fave;
    }

    public void start(Integer id) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(fave.getActivity()).build())
                .build();

        FavoriteApi favoriteApi = retrofit.create(FavoriteApi.class);

        Call<Favorite> call = favoriteApi.getBuyerFavorites(id);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Favorite> call, Response<Favorite> response) {
        if(response.isSuccessful()) {
            Favorite favorite = response.body();
            fave.response(favorite.getData(), response.message(), favorite.getStatusCode());
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<Favorite> call, Throwable t) {
        fave.fail(t);
    }
}
