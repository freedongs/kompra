package com.prototype.kompra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CustomFavorite {

    @SerializedName("data")
    @Expose
    private Favorite.Data data;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Favorite.Data getData() {
        return data;
    }

    public void setData(Favorite.Data data) {
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Data implements Serializable {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("BUYERID")
        @Expose
        private Integer bUYERID;
        @SerializedName("ProdID")
        @Expose
        private Integer prodID;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("product")
        @Expose
        private Product product;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getBUYERID() {
            return bUYERID;
        }

        public void setBUYERID(Integer bUYERID) {
            this.bUYERID = bUYERID;
        }

        public Integer getProdID() {
            return prodID;
        }

        public void setProdID(Integer prodID) {
            this.prodID = prodID;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }
    }
}
