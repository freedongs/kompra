package com.prototype.kompra.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prototype.kompra.R;
import com.prototype.kompra.adapter.FaveAdapter;
import com.prototype.kompra.controller.FavoriteController;
import com.prototype.kompra.model.Favorite;
import com.prototype.kompra.util.CallbackTemplateSingleObject;
import com.prototype.kompra.util.Session;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class Fave extends Fragment implements CallbackTemplateSingleObject {

    FavoriteController favoriteController;
    RecyclerView recyclerView;
    FaveAdapter faveAdapter;
    Session session;

    @Override
    public void onResume() {
        super.onResume();
        favoriteController = new FavoriteController(this);
        session = new Session(getActivity());
        try {
            favoriteController.start(session.getId(getActivity().getString(R.string.id)));
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fave, container, false);
        recyclerView = view.findViewById(R.id.fave_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
    }


    @Override
    public void response(Object result, String message, Integer statusCode) {
        recyclerView.setAdapter(null);
        Favorite.Data data = (Favorite.Data) result;
        faveAdapter = new FaveAdapter(data.getFavorite());
        recyclerView.setAdapter(faveAdapter);
    }

    @Override
    public void fail(Throwable stackTrace) {

    }
}
