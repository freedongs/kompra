package com.prototype.kompra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomOrderMasterlist {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("inserted_id")
    @Expose
    private Object insertedId;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Object getInsertedId() {
        return insertedId;
    }

    public void setInsertedId(Object insertedId) {
        this.insertedId = insertedId;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {

        @SerializedName("TotAmount")
        @Expose
        private String totAmount;
        @SerializedName("PaymentType")
        @Expose
        private String paymentType;
        @SerializedName("DeliveryDate")
        @Expose
        private String deliveryDate;
        @SerializedName("ShippingAdd")
        @Expose
        private String shippingAdd;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("OrMasID")
        @Expose
        private Integer orMasID;

        public String getTotAmount() {
            return totAmount;
        }

        public void setTotAmount(String totAmount) {
            this.totAmount = totAmount;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getDeliveryDate() {
            return deliveryDate;
        }

        public void setDeliveryDate(String deliveryDate) {
            this.deliveryDate = deliveryDate;
        }

        public String getShippingAdd() {
            return shippingAdd;
        }

        public void setShippingAdd(String shippingAdd) {
            this.shippingAdd = shippingAdd;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getOrMasID() {
            return orMasID;
        }

        public void setOrMasID(Integer orMasID) {
            this.orMasID = orMasID;
        }

    }
}
