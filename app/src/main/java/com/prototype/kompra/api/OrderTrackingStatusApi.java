package com.prototype.kompra.api;

import com.prototype.kompra.model.OrderTrackingStatus;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface OrderTrackingStatusApi {

    @GET("ordertrackingstatuses")
    Call<OrderTrackingStatus> getAllOrderTrackingStatus();

    @GET("ordertrackingstatuses/{id}")
    Call<OrderTrackingStatus> getOrderTrackingStatus(@Path("id") int id);

    @POST("ordertrackingstatuses")
    Call<OrderTrackingStatus> addOrderTrackingStatus(@Body OrderTrackingStatus orderTrackingStatus);

    @POST("ordertrackingstatuses-delete/{id}")
    Call<OrderTrackingStatus> removeOrderTrackingStatus(@Path("id") int id);

    @POST("ordertrackingstatuses/{id}")
    Call<OrderTrackingStatus> updateOrderTrackingStatus(@Path("id") int id, @Body OrderTrackingStatus orderTrackingStatus);
}
