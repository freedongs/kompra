package com.prototype.kompra.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.prototype.kompra.R;
import com.prototype.kompra.controller.CustomOrderMasterlistController;
import com.prototype.kompra.controller.FeedbackController;
import com.prototype.kompra.controller.ResponseUpdateController;
import com.prototype.kompra.model.CustomOrderMasterlist;
import com.prototype.kompra.model.FeedbackAndRate;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.model.Update;
import com.prototype.kompra.util.CallbackResponseTemplate;
import com.prototype.kompra.util.CallbackTemplateSingleObject;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;

public class FeedbackActivity extends AppCompatActivity implements CallbackTemplateSingleObject {


    Button feedback;
    FeedbackController feedbackController;
    RatingBar rating;
    EditText edt_feedback;
    OrderMasterlist.Datum datum;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_and_rate);
        getSupportActionBar().setTitle("Feedback and Rate");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rating = findViewById(R.id.rating);
        edt_feedback = findViewById(R.id.edt_feedback);
        feedback = findViewById(R.id.feedback);
        datum = (OrderMasterlist.Datum) getIntent().getSerializableExtra("feedback");

        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedbackController = new FeedbackController(FeedbackActivity.this);
                FeedbackAndRate.Data feedbackAndRate = new FeedbackAndRate().new Data();
                feedbackAndRate.setORDERRATE(Double.parseDouble(String.valueOf(rating.getRating())));
                feedbackAndRate.setORDERFEEDBACKS(edt_feedback.getText().toString());
                try {
                    feedbackController.start(feedbackAndRate);
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }


    @Override
    public void response(Object result, String message, Integer statusCode) {

        if (result instanceof Boolean) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(FeedbackActivity.this);
            builder1.setMessage("Thank you! For providing your feedback.");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();

            alert11.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Intent i = new Intent(FeedbackActivity.this, HomePageActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
            });
        } else {
            FeedbackAndRate.Data data = (FeedbackAndRate.Data) result;
            datum.setFEEDRATEID(data.getFEEDRATEID());
            ResponseUpdateController responseUpdateController = new ResponseUpdateController(FeedbackActivity.this);
            try {
                responseUpdateController.start();
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            responseUpdateController.update(datum);
        }
    }

    @Override
    public void fail(Throwable stackTrace) {

    }
}
