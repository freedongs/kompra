package com.prototype.kompra.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prototype.kompra.R;
import com.prototype.kompra.adapter.ClaimOrderAdapter;
import com.prototype.kompra.adapter.OrderDetailProductsAdapter;
import com.prototype.kompra.controller.CustomOrderMasterlistController;
import com.prototype.kompra.controller.OrderController;
import com.prototype.kompra.controller.ResponseUpdateController;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.util.CallbackTemplateSingleObject;
import com.prototype.kompra.util.Session;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class ClaimActivity extends AppCompatActivity implements CallbackTemplateSingleObject {

    OrderMasterlist.Datum datum;
    TextView order_number, order_total, payment_type, payment_date, address, status, buyer_name, contact_no;
    LinearLayout background;
    RecyclerView order_products_recycler_view;
    ClaimOrderAdapter claimOrderAdapter;
    Button claim;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claim);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        order_number = findViewById(R.id.order_number);
        buyer_name = findViewById(R.id.buyer_name);
        contact_no = findViewById(R.id.contact_no);
        order_total = findViewById(R.id.order_total);
        payment_type = findViewById(R.id.payment_type);
        payment_date = findViewById(R.id.payment_date);
        address = findViewById(R.id.address);
        status = findViewById(R.id.status);
        background = findViewById(R.id.status_linear);
        claim = findViewById(R.id.claim);
        order_products_recycler_view = findViewById(R.id.order_products_recycler_view);
        order_products_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        order_products_recycler_view.setItemAnimator(new DefaultItemAnimator());

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("container");
        datum = (OrderMasterlist.Datum) args.getSerializable("order");

        getSupportActionBar().setTitle("Claim");

        claimOrderAdapter = new ClaimOrderAdapter(datum.getOrderDetails());
        order_products_recycler_view.setAdapter(claimOrderAdapter);
        order_number.setText(datum.getOrMasID().toString());
        buyer_name.setText(datum.getBuyer().getBuyFname() + " " +datum.getBuyer().getBuyLname());
        contact_no.setText(datum.getBuyer().getBuyConNum());
        order_total.setText(datum.getTotAmount().toString());
        payment_type.setText(datum.getPaymentType());
        payment_date.setText(datum.getDeliveryDate());
        address.setText(datum.getShippingAdd());
        status.setText("Pending..");

        claim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Session session = new Session(ClaimActivity.this);
                datum.setEMPID(session.getId(v.getContext().getString(R.string.id)));
                datum.setOrTracID(1);
                ResponseUpdateController responseUpdateController = new ResponseUpdateController(ClaimActivity.this);
                try {
                    responseUpdateController.start();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                responseUpdateController.update(datum);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(ClaimActivity.this, RunnerHomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
        return true;
    }

    @Override
    public void response(Object result, String message, Integer statusCode) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(ClaimActivity.this);
        builder1.setMessage("Order has been claimed!");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

        alert11.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Intent i = new Intent(ClaimActivity.this, RunnerHomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
        alert11.show();
    }

    @Override
    public void fail(Throwable stackTrace) {
        Log.e("error",""+stackTrace.getMessage());
    }
}
