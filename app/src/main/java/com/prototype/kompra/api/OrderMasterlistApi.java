package com.prototype.kompra.api;

import com.prototype.kompra.model.CustomOrderMasterlist;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.model.Update;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface OrderMasterlistApi {

    @GET("ordermasterlists")
    Call<OrderMasterlist> getAllOrderMasterlist();

    @GET("ordermasterlists-runner-order")
    Call<OrderMasterlist> getAllOrderMasterlistRunnerOrder();

    @GET("ordermasterlists-runner-progress/{id}")
    Call<OrderMasterlist> getAllOrderMasterlistRunnerProgress(@Path("id") int id);

    @GET("ordermasterlists-runner-history/{id}")
    Call<OrderMasterlist> getAllOrderMasterlistRunnerHistory(@Path("id") int id);

    @GET("ordermasterlists/{id}")
    Call<OrderMasterlist> getOrderMasterlist(@Path("id") int id);

    @GET("ordermasterlists-buyer/{id}")
    Call<OrderMasterlist> getBuyerOrderMasterlist(@Path("id") int id);

    @GET("ordermasterlists-employee/{id}")
    Call<OrderMasterlist> getEmployeeOrderMasterlist(@Path("id") int id);

    @POST("ordermasterlists")
    Call<CustomOrderMasterlist> addOrderMasterlist(@Body OrderMasterlist.Datum datum);

    @POST("ordermasterlists-delete/{id}")
    Call<OrderMasterlist> removeOrderMasterlist(@Path("id") int id);

    @POST("ordermasterlists/{id}")
    Call<CustomOrderMasterlist> updateOrderMasterlist(@Path("id") int id, @Body OrderMasterlist.Datum orderMasterlist);

    @POST("ordermasterlists/{id}")
    Call<Update> updateOrderMasterlistRunner(@Path("id") int id, @Body OrderMasterlist.Datum orderMasterlist);
}
