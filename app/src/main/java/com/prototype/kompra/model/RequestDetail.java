package com.prototype.kompra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RequestDetail {

    public class Datum {

        @SerializedName("ReqDetID")
        @Expose
        private Integer reqDetID;
        @SerializedName("ItemReqName")
        @Expose
        private String itemReqName;
        @SerializedName("ItemReqDesc")
        @Expose
        private String itemReqDesc;
        @SerializedName("ItemReqPrice")
        @Expose
        private Double itemReqPrice;
        @SerializedName("ItemReqQuantity")
        @Expose
        private Integer itemReqQuantity;
        @SerializedName("approve")
        @Expose
        private Integer approve;
        @SerializedName("BUYERID")
        @Expose
        private Integer bUYERID;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getReqDetID() {
            return reqDetID;
        }

        public void setReqDetID(Integer reqDetID) {
            this.reqDetID = reqDetID;
        }

        public String getItemReqName() {
            return itemReqName;
        }

        public void setItemReqName(String itemReqName) {
            this.itemReqName = itemReqName;
        }

        public String getItemReqDesc() {
            return itemReqDesc;
        }

        public void setItemReqDesc(String itemReqDesc) {
            this.itemReqDesc = itemReqDesc;
        }

        public Double getItemReqPrice() {
            return itemReqPrice;
        }

        public void setItemReqPrice(Double itemReqPrice) {
            this.itemReqPrice = itemReqPrice;
        }

        public Integer getItemReqQuantity() {
            return itemReqQuantity;
        }

        public void setItemReqQuantity(Integer itemReqQuantity) {
            this.itemReqQuantity = itemReqQuantity;
        }

        public Integer getApprove() {
            return approve;
        }

        public void setApprove(Integer approve) {
            this.approve = approve;
        }

        public Integer getBUYERID() {
            return bUYERID;
        }

        public void setBUYERID(Integer bUYERID) {
            this.bUYERID = bUYERID;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }


    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }
}
