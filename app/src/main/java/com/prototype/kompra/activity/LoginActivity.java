package com.prototype.kompra.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.prototype.kompra.R;
import com.prototype.kompra.controller.UserController;
import com.prototype.kompra.model.User;
import com.prototype.kompra.util.CallbackTemplateSingleObject;
import com.prototype.kompra.util.Session;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class LoginActivity extends AppCompatActivity implements CallbackTemplateSingleObject {

    EditText email, password;
    TextView register_link, runner_link;
    Button login;
    Session session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.login);
        register_link = findViewById(R.id.register_link);
        runner_link = findViewById(R.id.runner_link);
        session = new Session(this);
        if (session.getUser(getString(R.string.user)) != null) {
            Intent i = new Intent(this, HomePageActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }

        getSupportActionBar().setTitle("Login");

        register_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        runner_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RunnerLoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserController userController = new UserController(LoginActivity.this);
                try {
                    userController.start();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                }
                User.Data data = new User().new Data();

                if(email.getText().toString().trim().length() == 0){
                    email.setError("Field must not be empty");
                    return;
                }
                if(password.getText().toString().trim().length() == 0){
                    password.setError("Field must not be empty");
                    return;
                }

                data.setEmail(email.getText().toString());
                data.setPassword(password.getText().toString());
                userController.login(data);
            }
        });
    }

    @Override
    public void response(Object result, String message, Integer statusCode) {
        User user = (User) result;

        if (user.getData().getUserType().equals("buyer") && user.getData().getEnabled()) {
            session.putId(this.getString(R.string.id), user.getData().getId());
            session.putToken(this.getString(R.string.token), user.getSuccess().getToken());
            session.putUser(this.getString(R.string.user), user);

            Intent i = new Intent(this, HomePageActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        } else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
            builder1.setMessage("Invalid user!");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }

    @Override
    public void fail(Throwable stackTrace) {

    }
}
