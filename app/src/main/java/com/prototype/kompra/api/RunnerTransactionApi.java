package com.prototype.kompra.api;

import com.prototype.kompra.model.RunnerTransaction;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RunnerTransactionApi {

    @GET("runnertransactions")
    Call<RunnerTransaction> getAllRunnerTransaction();

    @GET("runnertransactions/{id}")
    Call<RunnerTransaction> getRunnerTransaction(@Path("id") int id);

    @POST("runnertransactions")
    Call<RunnerTransaction> addRunnerTransaction(@Body RunnerTransaction runnerTransaction);

    @POST("runnertransactions-delete/{id}")
    Call<RunnerTransaction> removeRunnerTransaction(@Path("id") int id);

    @POST("runnertransactions/{id}")
    Call<RunnerTransaction> updateRunnerTransaction(@Path("id") int id, @Body RunnerTransaction runnerTransaction);
}
