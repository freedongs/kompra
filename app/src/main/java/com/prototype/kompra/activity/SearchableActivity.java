package com.prototype.kompra.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.GridView;

import com.prototype.kompra.R;
import com.prototype.kompra.adapter.ProductAdapter;
import com.prototype.kompra.controller.SearchController;
import com.prototype.kompra.model.Category;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.model.Search;
import com.prototype.kompra.util.CallbackResponseTemplate;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;

public class SearchableActivity extends AppCompatActivity implements CallbackResponseTemplate {

    public static String IS_CATEGORY = "category";

    GridView gridView;
    ProductAdapter productAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        getSupportActionBar().setTitle("Search");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gridView = findViewById(R.id.products_grid_view);

        Bundle appData = getIntent().getBundleExtra(SearchManager.APP_DATA);
        if (appData != null) {
        }

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Search search = new Search(query, false);
            SearchController searchController = new SearchController(this);
            try {
                searchController.start(search);
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(SearchableActivity.this, HomePageActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
        return true;
    }

    @Override
    public void response(List result, String message, Integer statusCode) {
        gridView.setAdapter(null);
        productAdapter = new ProductAdapter((List<Product.Datum>) result, this);
        gridView.setAdapter(productAdapter);
    }

    @Override
    public void fail(Throwable stackTrace) {
        System.out.println(stackTrace.getMessage());
    }
}
