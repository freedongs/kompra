package com.prototype.kompra.controller;

import android.support.v4.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.api.OrderMasterlistApi;
import com.prototype.kompra.fragment.RunnerHistory;
import com.prototype.kompra.fragment.RunnerInProgress;
import com.prototype.kompra.fragment.RunnerOrder;
import com.prototype.kompra.fragment.Transaction;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrderController implements Callback<OrderMasterlist> {

    private Gson gson;
    private Retrofit retrofit;
    private Fragment fragment;

    public OrderController(Fragment fragment) {
        this.fragment = fragment;
    }

    public void start() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(fragment.getContext()).build())
                .build();
    }
    @Override
    public void onResponse(Call<OrderMasterlist> call, Response<OrderMasterlist> response) {
        if(response.isSuccessful()) {
            OrderMasterlist orderMasterlist = response.body();
            if (fragment instanceof Transaction) {
                ((Transaction)fragment).response(orderMasterlist.getData(), response.message(), orderMasterlist.getStatusCode());
            } else if (fragment instanceof RunnerOrder) {
                ((RunnerOrder)fragment).response(orderMasterlist.getData(), response.message(), orderMasterlist.getStatusCode());
            } else if (fragment instanceof RunnerInProgress) {
                ((RunnerInProgress)fragment).response(orderMasterlist.getData(), response.message(), orderMasterlist.getStatusCode());
            } else if (fragment instanceof RunnerHistory) {
                ((RunnerHistory)fragment).response(orderMasterlist.getData(), response.message(), orderMasterlist.getStatusCode());
            }
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<OrderMasterlist> call, Throwable t) {
        if (fragment instanceof Transaction) {
            ((Transaction)fragment).fail(t);
        } else if (fragment instanceof RunnerOrder) {
            ((RunnerOrder)fragment).fail(t);
        } else if (fragment instanceof RunnerInProgress) {
            ((RunnerInProgress)fragment).fail(t);
        } else if (fragment instanceof RunnerHistory) {
            ((RunnerHistory)fragment).fail(t);
        }
    }

    public void getOrders(Integer id){

        OrderMasterlistApi orderMasterlistApi = retrofit.create(OrderMasterlistApi.class);

        Call<OrderMasterlist> call = orderMasterlistApi.getBuyerOrderMasterlist(id);
        call.enqueue(this);
    }

    public void getOrder(){

        OrderMasterlistApi orderMasterlistApi = retrofit.create(OrderMasterlistApi.class);

        Call<OrderMasterlist> call = orderMasterlistApi.getAllOrderMasterlist();
        call.enqueue(this);
    }

    public void getOrderRunnerOrder(){

        OrderMasterlistApi orderMasterlistApi = retrofit.create(OrderMasterlistApi.class);

        Call<OrderMasterlist> call = orderMasterlistApi.getAllOrderMasterlistRunnerOrder();
        call.enqueue(this);
    }

    public void getEmployeeOrder(Integer id){

        OrderMasterlistApi orderMasterlistApi = retrofit.create(OrderMasterlistApi.class);

        Call<OrderMasterlist> call = orderMasterlistApi.getEmployeeOrderMasterlist(id);
        call.enqueue(this);
    }

    public void getEmployeeProgress(Integer id){

        OrderMasterlistApi orderMasterlistApi = retrofit.create(OrderMasterlistApi.class);

        Call<OrderMasterlist> call = orderMasterlistApi.getAllOrderMasterlistRunnerProgress(id);
        call.enqueue(this);
    }

    public void getEmployeeHistory(Integer id){

        OrderMasterlistApi orderMasterlistApi = retrofit.create(OrderMasterlistApi.class);

        Call<OrderMasterlist> call = orderMasterlistApi.getAllOrderMasterlistRunnerHistory(id);
        call.enqueue(this);
    }
}
