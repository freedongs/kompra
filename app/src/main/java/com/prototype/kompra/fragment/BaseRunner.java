package com.prototype.kompra.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prototype.kompra.R;
import com.prototype.kompra.adapter.ViewPagerAdapter;

public class BaseRunner extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base_runner, container, false);

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setText("Orders");
        tabLayout.getTabAt(1).setText("In Progress");
        tabLayout.getTabAt(2).setText("History");

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Orders");

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch(tab.getPosition()) {
                    case 0:
                        viewPager.setCurrentItem(0);
                        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Orders");
                        break;
                    case 1:
                        viewPager.setCurrentItem(1);
                        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("In Progress");
                        break;
                    case 2:
                        viewPager.setCurrentItem(2);
                        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("History");
                        break;

                    default:
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return view;

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new RunnerOrder(), "Orders");
        adapter.addFragment(new RunnerInProgress(), "In Progress");
        adapter.addFragment(new RunnerHistory(), "History");
        viewPager.setAdapter(adapter);
    }
}
