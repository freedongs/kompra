package com.prototype.kompra.api;

import com.prototype.kompra.model.Promo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PromoApi {

    @GET("promocodesearch/{id}")
    Call<Promo> getPromo(@Path("id") String id);
}
