package com.prototype.kompra.adapter;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prototype.kompra.R;
import com.prototype.kompra.activity.HomePageActivity;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.model.RequestDetail;
import com.prototype.kompra.util.Session;

import java.util.ArrayList;
import java.util.List;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ProductViewHolder> {

    List<RequestDetail.Datum> data;

    public RequestAdapter(List<RequestDetail.Datum> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_request_item_recycler_view, viewGroup, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder productViewHolder, int i) {
        RequestDetail.Datum object = data.get(i);
        productViewHolder.productName.setText(object.getItemReqName());
        productViewHolder.productDescription.setText(object.getItemReqDesc());
        productViewHolder.productPrize.setText(object.getItemReqPrice().toString());
        productViewHolder.productQuantity.setText(object.getItemReqQuantity().toString());
        if(object.getApprove() == 1){
            productViewHolder.status.setText("Approved");
        } else {
            productViewHolder.status.setText("Pending..");
        }
        productViewHolder.datum = object;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView productPic;
        public TextView productName;
        public TextView productDescription;
        public TextView productPrize;
        public TextView productQuantity;
        public TextView status;
        public RequestDetail.Datum datum;
        public Button edit_request, cancel_request;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);

            productPic = (ImageView) itemView.findViewById(R.id.product_image);
            productName = (TextView) itemView.findViewById(R.id.product_name);
            productDescription = (TextView) itemView.findViewById(R.id.product_description);
            productPrize = (TextView) itemView.findViewById(R.id.product_prize);
            productQuantity = (TextView) itemView.findViewById(R.id.product_quantity);
            status = (TextView) itemView.findViewById(R.id.status);
            edit_request = itemView.findViewById(R.id.edit_request);
            cancel_request = itemView.findViewById(R.id.cancel_request);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Dialog dialog = new Dialog(v.getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(true);

            View view = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_view_product, null);

            TextView productName = view.findViewById(R.id.product_name);
            TextView productDesc = view.findViewById(R.id.product_description);
            TextView productPrize = view.findViewById(R.id.product_prize);
            TextView productQuantity = view.findViewById(R.id.product_quantity);
            TextView status = view.findViewById(R.id.status);
            LinearLayout layout_status = view.findViewById(R.id.layout_status);

            productName.setText(datum.getItemReqName());
            productDesc.setText(datum.getItemReqDesc());
            productPrize.setText(datum.getItemReqPrice().toString());
            productQuantity.setText(datum.getItemReqQuantity().toString());
            layout_status.setVisibility(View.VISIBLE);
            if(datum.getApprove() == 1){
                status.setText("Approved");
            } else {
                status.setText("Pending..");
            }

            dialog.setContentView(view);

            dialog.show();
        }
    }
}
