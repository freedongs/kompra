package com.prototype.kompra.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.prototype.kompra.R;
import com.prototype.kompra.adapter.ClaimOrderAdapter;
import com.prototype.kompra.controller.ResponseUpdateController;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.util.CallbackTemplateSingleObject;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class InProgressActivity extends AppCompatActivity implements CallbackTemplateSingleObject {

    OrderMasterlist.Datum datum;
    TextView order_number, order_total, payment_type, payment_date, address, status, buyer_name, contact_no;
    LinearLayout background, in_progress_linear_layout;
    RecyclerView order_products_recycler_view;
    ClaimOrderAdapter claimOrderAdapter;
    Button start, deliver, received;
    ProgressBar progress_bar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_progress);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        order_number = findViewById(R.id.order_number);
        buyer_name = findViewById(R.id.buyer_name);
        contact_no = findViewById(R.id.contact_no);
        order_total = findViewById(R.id.order_total);
        payment_type = findViewById(R.id.payment_type);
        payment_date = findViewById(R.id.payment_date);
        address = findViewById(R.id.address);
        status = findViewById(R.id.status);
        background = findViewById(R.id.status_linear);
        start = findViewById(R.id.start);
        deliver = findViewById(R.id.deliver);
        received = findViewById(R.id.received);
        progress_bar = findViewById(R.id.progress_bar);
        in_progress_linear_layout = findViewById(R.id.in_progress_linear_layout);
        order_products_recycler_view = findViewById(R.id.order_products_recycler_view);
        order_products_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        order_products_recycler_view.setItemAnimator(new DefaultItemAnimator());

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("container");
        datum = (OrderMasterlist.Datum) args.getSerializable("order");

        getSupportActionBar().setTitle("In progress");

        claimOrderAdapter = new ClaimOrderAdapter(datum.getOrderDetails());
        order_products_recycler_view.setAdapter(claimOrderAdapter);
        order_number.setText(datum.getOrMasID().toString());
        buyer_name.setText(datum.getBuyer().getBuyFname() + " " +datum.getBuyer().getBuyLname());
        contact_no.setText(datum.getBuyer().getBuyConNum());
        order_total.setText(datum.getTotAmount().toString());
        payment_type.setText(datum.getPaymentType());
        payment_date.setText(datum.getDeliveryDate());
        address.setText(datum.getShippingAdd());

        if (datum.getOrTracID() == null) {
            status.setText("Waiting for a runner...");
        } else if(datum.getOrTracID() == 1){
            background.setBackgroundResource(R.drawable.background_pending);
            status.setText("Pending..");
        } else if(datum.getOrTracID() == 2){
            status.setText("Processing");
            background.setBackgroundResource(R.drawable.background_processing);
        } else if(datum.getOrTracID() == 3){
            status.setText("On the way...");
            background.setBackgroundResource(R.drawable.background_on_d_way);
        } else if(datum.getOrTracID() == 4){
            status.setText("Received.");
            background.setBackgroundResource(R.drawable.background_received);
        }

        if(datum.getOrTracID()==1){
            start.setVisibility(View.VISIBLE);
        }
        else if (datum.getOrTracID()==2) {
            start.setVisibility(View.GONE);
            deliver.setVisibility(View.VISIBLE);
        } else if (datum.getOrTracID()==3) {
            deliver.setVisibility(View.GONE);
            received.setVisibility(View.VISIBLE);
        }
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datum.setOrTracID(2);
                ResponseUpdateController responseUpdateController = new ResponseUpdateController(InProgressActivity.this);
                try {
                    responseUpdateController.start();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                responseUpdateController.update(datum);
                start.setVisibility(View.GONE);
                deliver.setVisibility(View.VISIBLE);
                progress_bar.setVisibility(View.VISIBLE);
                in_progress_linear_layout.setVisibility(View.GONE);
                status.setText("Processing");
                background.setBackgroundResource(R.drawable.background_processing);
            }
        });

        deliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datum.setOrTracID(3);
                ResponseUpdateController responseUpdateController = new ResponseUpdateController(InProgressActivity.this);
                try {
                    responseUpdateController.start();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                responseUpdateController.update(datum);
                deliver.setVisibility(View.GONE);
                received.setVisibility(View.VISIBLE);
                progress_bar.setVisibility(View.VISIBLE);
                in_progress_linear_layout.setVisibility(View.GONE);
                status.setText("On the way...");
                background.setBackgroundResource(R.drawable.background_on_d_way);
            }
        });

        received.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datum.setOrTracID(4);
                ResponseUpdateController responseUpdateController = new ResponseUpdateController(InProgressActivity.this);
                try {
                    responseUpdateController.start();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                responseUpdateController.update(datum);
                received.setVisibility(View.GONE);
                progress_bar.setVisibility(View.VISIBLE);
                in_progress_linear_layout.setVisibility(View.GONE);
                status.setText("Received.");
                background.setBackgroundResource(R.drawable.background_received);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(InProgressActivity.this, RunnerHomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
        return true;
    }


    @Override
    public void response(Object result, String message, Integer statusCode) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(InProgressActivity.this);
        builder1.setMessage("Good job!");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        progress_bar.setVisibility(View.GONE);
                        in_progress_linear_layout.setVisibility(View.VISIBLE);
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void fail(Throwable stackTrace) {

    }
}
