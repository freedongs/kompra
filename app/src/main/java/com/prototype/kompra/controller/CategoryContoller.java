package com.prototype.kompra.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.api.CategoryApi;
import com.prototype.kompra.fragment.Home;
import com.prototype.kompra.model.Category;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CategoryContoller implements Callback<Category> {

    private Home home;

    public CategoryContoller(Home home) {

        this.home = home;
    }

    public void start() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(home.getContext()).build())
                .build();

        CategoryApi categoryApi = retrofit.create(CategoryApi.class);

        Call<Category> call = categoryApi.getAllCategoryProduct();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Category> call, Response<Category> response) {

        if(response.isSuccessful()) {
            Category resultResponse = response.body();
            home.response(resultResponse.getData(), response.message(),resultResponse.getStatusCode());
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<Category> call, Throwable t) {

        home.fail(t);
    }
}
