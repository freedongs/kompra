package com.prototype.kompra.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.prototype.kompra.R;
import com.prototype.kompra.activity.LoginActivity;
import com.prototype.kompra.activity.PaymentAndShippingActvity;
import com.prototype.kompra.activity.RegistrationActivity;
import com.prototype.kompra.adapter.CartAdapter;
import com.prototype.kompra.controller.PromoController;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.model.Promo;
import com.prototype.kompra.util.CallbackResponseTemplate;
import com.prototype.kompra.util.Session;

import java.io.IOException;
import java.io.Serializable;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

public class Cart extends Fragment implements CallbackResponseTemplate {

    RecyclerView recyclerView;
    CartAdapter cartAdapter;
    List<Product.Datum> data;
    LinearLayout cart_checkout_view;
    EditText promo_code;
    Button checkout;

    @Override
    public void onResume() {
        super.onResume();
        cartAdapter= new CartAdapter(data, this);
        cartAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(cartAdapter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        recyclerView = view.findViewById(R.id.products_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        checkout = view.findViewById(R.id.checkout);
        cart_checkout_view = view.findViewById(R.id.cart_checkout_view);
        promo_code = view.findViewById(R.id.promo_code);

        Session session = new Session(getActivity());
        if (session.getSharedPreferences().contains(view.getContext().getString(R.string.cart)) && session.getProducts(view.getContext().getString(R.string.cart)).size() > 0) {
            data = new ArrayList<>(session.getProducts(view.getContext().getString(R.string.cart)));
            cart_checkout_view.setVisibility(View.VISIBLE);
        } else {
            cart_checkout_view.setVisibility(View.GONE);
            data = new ArrayList<>();
        }

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( promo_code.getText().toString().isEmpty()) {
                    Intent i = new Intent(getActivity(), PaymentAndShippingActvity.class);
                    i.putExtra("products", (Serializable) data);
                    i.putExtra("promo", new ArrayList<>());
                    startActivity(i);
                } else {
                    PromoController promoController = new PromoController(Cart.this);
                    try {
                        promoController.start(promo_code.getText().toString());
                    } catch (CertificateException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (KeyStoreException e) {
                        e.printStackTrace();
                    } catch (KeyManagementException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        return view;
    }

    public void checkIfCartEmpty(List<Product.Datum> cart) {

        if (cart.size() == 0) {
            checkout.setVisibility(View.GONE);
        } else {
            checkout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void response(List result, String message, Integer statusCode) {
        final List<Promo.Datum> promo = new ArrayList<>((List<Promo.Datum>) result);
        promo_code.setText("");

        if (promo.size() > 0){
            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
            builder1.setMessage("Promo code found! Do you wish to apply promo? ");
            builder1.setCancelable(true);
            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alert11 = builder1.create();
            alert11.show();

            alert11.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Intent i = new Intent(getActivity(), PaymentAndShippingActvity.class);
                    i.putExtra("products", (Serializable) data);
                    i.putExtra("promo", (Serializable) promo);
                    startActivity(i);
                }
            });
        } else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
            builder1.setMessage("Promo code not found!. Do wish to continue? ");
            builder1.setCancelable(true);
            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alert11 = builder1.create();
            alert11.show();

            alert11.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Intent i = new Intent(getActivity(), PaymentAndShippingActvity.class);
                    i.putExtra("products", (Serializable) data);
                    i.putExtra("promo", (Serializable) promo);
                    startActivity(i);
                }
            });
        }
    }

    @Override
    public void fail(Throwable stackTrace) {

    }
}
