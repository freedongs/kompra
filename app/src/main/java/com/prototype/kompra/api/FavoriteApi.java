package com.prototype.kompra.api;

import com.prototype.kompra.model.CustomFavorite;
import com.prototype.kompra.model.Favorite;
import com.prototype.kompra.model.Update;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface FavoriteApi {

    @GET("favorites")
    Call<Favorite> getAllFavorite();

    @GET("favorites/{id}")
    Call<Favorite> getFavorite(@Path("id") int id);

    @GET("buyers-favorites/{id}")
    Call<Favorite> getBuyerFavorites(@Path("id") int id);

    @POST("favorites")
    Call<CustomFavorite> addFavorite(@Body Favorite.Data2 favorite);

    @POST("favorites-delete/{id}")
    Call<Update> removeFavorite(@Path("id") int id);

    @POST("favorites/{id}")
    Call<Favorite.Data2> updateFavorite(@Path("id") int id, @Body Favorite.Data2 favorite);
}
