package com.prototype.kompra.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.activity.SearchableActivity;
import com.prototype.kompra.api.SearchApi;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.model.Search;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchController implements Callback<Product> {

    private SearchableActivity searchableActivity;

    public SearchController(SearchableActivity searchableActivity) {
        this.searchableActivity = searchableActivity;
    }

    public void start(Search search) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(searchableActivity).build())
                .build();

        SearchApi searchApi = retrofit.create(SearchApi.class);

        Call<Product> call = searchApi.search(search);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Product> call, Response<Product> response) {

        if(response.isSuccessful()) {
            Product resultResponse = response.body();
            searchableActivity.response(resultResponse.getData(), response.message(),resultResponse.getStatusCode());
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<Product> call, Throwable t) {
        searchableActivity.fail(t);
    }
}
