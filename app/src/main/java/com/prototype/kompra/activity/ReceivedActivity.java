package com.prototype.kompra.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prototype.kompra.R;
import com.prototype.kompra.adapter.OrderDetailProductsAdapter;
import com.prototype.kompra.adapter.ReceivedProductsAdapter;
import com.prototype.kompra.model.OrderMasterlist;

public class ReceivedActivity extends AppCompatActivity {

    OrderMasterlist.Datum datum;
    TextView order_number, order_total, payment_type, payment_date, address, status, buyer_name, contact_no;
    LinearLayout background;
    RecyclerView order_products_recycler_view;
    ReceivedProductsAdapter receivedProductsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_received);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        order_number = findViewById(R.id.order_number);
        buyer_name = findViewById(R.id.buyer_name);
        contact_no = findViewById(R.id.contact_no);
        order_total = findViewById(R.id.order_total);
        payment_type = findViewById(R.id.payment_type);
        payment_date = findViewById(R.id.payment_date);
        address = findViewById(R.id.address);
        status = findViewById(R.id.status);
        background = findViewById(R.id.status_linear);
        order_products_recycler_view = findViewById(R.id.order_products_recycler_view);
        order_products_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        order_products_recycler_view.setItemAnimator(new DefaultItemAnimator());

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("container");
        datum = (OrderMasterlist.Datum) args.getSerializable("order");

        getSupportActionBar().setTitle("All transactions");

        receivedProductsAdapter = new ReceivedProductsAdapter(datum.getOrderDetails());
        order_products_recycler_view.setAdapter(receivedProductsAdapter);
        order_number.setText(datum.getOrMasID().toString());
        buyer_name.setText(datum.getBuyer().getBuyFname() + " " +datum.getBuyer().getBuyLname());
        contact_no.setText(datum.getBuyer().getBuyConNum());
        order_total.setText(datum.getTotAmount().toString());
        payment_type.setText(datum.getPaymentType());
        payment_date.setText(datum.getDeliveryDate());
        address.setText(datum.getShippingAdd());
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(ReceivedActivity.this, RunnerHomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
        return true;
    }
}
