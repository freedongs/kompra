package com.prototype.kompra.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.prototype.kompra.R;
import com.prototype.kompra.controller.UserController;
import com.prototype.kompra.model.User;
import com.prototype.kompra.util.CallbackTemplateSingleObject;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class RegistrationActivity extends AppCompatActivity implements CallbackTemplateSingleObject {

    EditText firstName, lastName, address, occupation, gender, age, dob, email, password, confirm_password, license_number, contact_number, senior_citizen;
    private RadioGroup radioPWD;
    Button register;
    Calendar myCalendar = Calendar.getInstance();
    User.Data data = new User().new Data();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        address = (EditText) findViewById(R.id.address);
        occupation = (EditText) findViewById(R.id.occupation);
        gender = (EditText) findViewById(R.id.gender);
        age = (EditText) findViewById(R.id.age);
        dob = (EditText) findViewById(R.id.dob);
        contact_number = (EditText) findViewById(R.id.contact_number);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        confirm_password = (EditText) findViewById(R.id.confirm_password);
        license_number = (EditText) findViewById(R.id.license_number);
        senior_citizen = (EditText) findViewById(R.id.senior_citizen);
        radioPWD = (RadioGroup) findViewById(R.id.radioPWD);
        register = (Button) findViewById(R.id.register);

        getSupportActionBar().setTitle("Register");

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.user_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(v.getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserController userController = new UserController(RegistrationActivity.this);
                try {
                    userController.start();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                }
                data.setFirstName(firstName.getText().toString());
                data.setLastName(lastName.getText().toString());
                data.setAddress(address.getText().toString());
                data.setOccupation(occupation.getText().toString());
                data.setGender(gender.getText().toString());
                data.setAge(Integer.parseInt(age.getText().toString()));
                data.setDob(dob.getText().toString());
                data.setEmail(email.getText().toString());
                data.setPassword(password.getText().toString());
                data.setC_password(confirm_password.getText().toString());
                data.setBuyConNum(contact_number.getText().toString());
                data.setSeniorCitizenID(senior_citizen.getText().toString());
                data.setUserType("buyer");
                int selectedId = radioPWD.getCheckedRadioButtonId();

                if(selectedId == R.id.radio_yes){
                    data.setEnabled(false);
                    data.setPwd(true);
                } else {
                    data.setEnabled(true);
                    data.setPwd(false);
                }

                if(firstName.getText().toString().trim().length() == 0){
                    firstName.setError("Field must not be empty");
                    return;
                }
                if(lastName.getText().toString().trim().length() == 0){
                    lastName.setError("Field must not be empty");
                    return;
                }
                if(address.getText().toString().trim().length() == 0){
                    address.setError("Field must not be empty");
                    return;
                }
                if(occupation.getText().toString().trim().length() == 0){
                    occupation.setError("Field must not be empty");
                    return;
                }
                if(gender.getText().toString().trim().length() == 0){
                    gender.setError("Field must not be empty");
                    return;
                }
                if(age.getText().toString().trim().length() == 0){
                    age.setError("Field must not be empty");
                    return;
                }
                if(dob.getText().toString().trim().length() == 0){
                    dob.setError("Field must not be empty");
                    return;
                }
                if(email.getText().toString().trim().length() == 0){
                    email.setError("Field must not be empty");
                    return;
                }
                if(password.getText().toString().trim().length() == 0){
                    password.setError("Field must not be empty");
                    return;
                }
                if(confirm_password.getText().toString().trim().length() == 0){
                    confirm_password.setError("Field must not be empty");
                    return;
                }
                if(contact_number.getText().toString().trim().length() == 0){
                    contact_number.setError("Field must not be empty");
                    return;
                }


                userController.register(data);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();

        return true;
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dob.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void response(Object result, String message, Integer statusCode) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(RegistrationActivity.this);
        builder1.setMessage("Succesfully registered!");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

        alert11.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }

    @Override
    public void fail(Throwable stackTrace) {

    }
}
