package com.prototype.kompra.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prototype.kompra.R;
import com.prototype.kompra.adapter.RunnerOrderAdapter;
import com.prototype.kompra.controller.OrderController;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.util.CallbackResponseTemplate;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;

public class RunnerOrder extends Fragment implements CallbackResponseTemplate {

    RecyclerView runner_order;
    RunnerOrderAdapter runnerOrderAdapter;
    OrderController orderController;

    @Override
    public void onResume() {
        super.onResume();
        orderController = new OrderController(this);
        try {
            orderController.start();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        orderController.getOrderRunnerOrder();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        runner_order = view.findViewById(R.id.runner_order);
        runner_order.setLayoutManager(new LinearLayoutManager(getActivity()));
        runner_order.setItemAnimator(new DefaultItemAnimator());
        return  view;
    }

    @Override
    public void response(List result, String message, Integer statusCode) {
        runner_order.setAdapter(null);
        runnerOrderAdapter = new RunnerOrderAdapter((List<OrderMasterlist.Datum>) result);
        runner_order.setAdapter(runnerOrderAdapter);
    }

    @Override
    public void fail(Throwable stackTrace) {

    }
}
