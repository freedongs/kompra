package com.prototype.kompra.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.prototype.kompra.R;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.model.User;

import java.lang.reflect.Type;
import java.util.List;

public class Session {

    SharedPreferences sharedPreferences;

    public Session(Context context) {
        this.sharedPreferences = context.getSharedPreferences(context.getString(R.string.kompra_key), Context.MODE_PRIVATE);
    }

    public void putUser(String key, User user) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        set(key, json);
    }

    public User getUser(String key) {
        if (sharedPreferences != null) {

            Gson gson = new Gson();
            User data;

            String string = sharedPreferences.getString(key, null);
            Type type = new TypeToken<User>() {
            }.getType();
            data = gson.fromJson(string, type);
            return data;
        }
        return null;
    }

    public void addCart(String key, List<Product.Datum> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        set(key, json);
    }

    public void set(String key, String value) {
        if (sharedPreferences != null) {
            SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
            prefsEditor.putString(key, value);
            prefsEditor.apply();
        }
    }

    public List<Product.Datum> getProducts(String key) {
        if (sharedPreferences != null) {

            Gson gson = new Gson();
            List<Product.Datum> data;

            String string = sharedPreferences.getString(key, null);
            Type type = new TypeToken<List<Product.Datum>>() {
            }.getType();
            data = gson.fromJson(string, type);
            return data;
        }
        return null;
    }

    public void putToken(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public void putId(String key, Integer value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public String getToken(String key) {
        return sharedPreferences.getString(key, null);
    }

    public Integer getId(String key) {
        return sharedPreferences.getInt(key, -1);
    }

    public void clearSession(String sessionId) {
        sharedPreferences.edit().remove(sessionId).apply();
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public void setSharedPreferences(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }
}
