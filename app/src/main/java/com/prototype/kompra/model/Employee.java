package com.prototype.kompra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Employee implements Serializable {

    @SerializedName("EMPID")
    @Expose
    private Integer eMPID;
    @SerializedName("EMPFname")
    @Expose
    private String eMPFname;
    @SerializedName("EMPLname")
    @Expose
    private String eMPLname;
    @SerializedName("EMPAdd")
    @Expose
    private String eMPAdd;
    @SerializedName("EMPConNum")
    @Expose
    private String eMPConNum;
    @SerializedName("EMPAge")
    @Expose
    private Integer eMPAge;
    @SerializedName("EMPPosi")
    @Expose
    private String eMPPosi;
    @SerializedName("EMPDoB")
    @Expose
    private String eMPDoB;
    @SerializedName("DriverID")
    @Expose
    private Integer driverID;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("driver_detail")
    @Expose
    private DriverDetail driverDetail;

    public Integer getEMPID() {
        return eMPID;
    }

    public void setEMPID(Integer eMPID) {
        this.eMPID = eMPID;
    }

    public String getEMPFname() {
        return eMPFname;
    }

    public void setEMPFname(String eMPFname) {
        this.eMPFname = eMPFname;
    }

    public String getEMPLname() {
        return eMPLname;
    }

    public void setEMPLname(String eMPLname) {
        this.eMPLname = eMPLname;
    }

    public String getEMPAdd() {
        return eMPAdd;
    }

    public void setEMPAdd(String eMPAdd) {
        this.eMPAdd = eMPAdd;
    }

    public String getEMPConNum() {
        return eMPConNum;
    }

    public void setEMPConNum(String eMPConNum) {
        this.eMPConNum = eMPConNum;
    }

    public Integer getEMPAge() {
        return eMPAge;
    }

    public void setEMPAge(Integer eMPAge) {
        this.eMPAge = eMPAge;
    }

    public String getEMPPosi() {
        return eMPPosi;
    }

    public void setEMPPosi(String eMPPosi) {
        this.eMPPosi = eMPPosi;
    }

    public String getEMPDoB() {
        return eMPDoB;
    }

    public void setEMPDoB(String eMPDoB) {
        this.eMPDoB = eMPDoB;
    }

    public Integer getDriverID() {
        return driverID;
    }

    public void setDriverID(Integer driverID) {
        this.driverID = driverID;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public DriverDetail getDriverDetail() {
        return driverDetail;
    }

    public void setDriverDetail(DriverDetail driverDetail) {
        this.driverDetail = driverDetail;
    }

}
