package com.prototype.kompra.controller;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.activity.ClaimActivity;
import com.prototype.kompra.activity.FeedbackActivity;
import com.prototype.kompra.activity.InProgressActivity;
import com.prototype.kompra.activity.OrderActivity;
import com.prototype.kompra.activity.PaymentAndShippingActvity;
import com.prototype.kompra.activity.UpdateUserActivity;
import com.prototype.kompra.adapter.FaveAdapter;
import com.prototype.kompra.adapter.OrderDetailProductsAdapter;
import com.prototype.kompra.api.FavoriteApi;
import com.prototype.kompra.api.OrderDetailApi;
import com.prototype.kompra.api.OrderMasterlistApi;
import com.prototype.kompra.api.UserApi;
import com.prototype.kompra.model.OrderDetail;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.model.Update;
import com.prototype.kompra.model.User;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResponseUpdateController implements Callback<Update> {

    private Gson gson;
    private Retrofit retrofit;
    private AppCompatActivity appCompatActivity;
    private FaveAdapter faveAdapter;
    private OrderDetailProductsAdapter.ProductViewHolder orderDetailProductsAdapter;
    Context context;

    public ResponseUpdateController(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    public ResponseUpdateController(FaveAdapter faveAdapter, Context context) {
        this.faveAdapter = faveAdapter;
        this.context = context;
    }

    public ResponseUpdateController(OrderDetailProductsAdapter.ProductViewHolder orderDetailProductsAdapter, Context context) {
        this.orderDetailProductsAdapter = orderDetailProductsAdapter;
        this.context = context;
    }

    public void start() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {

        gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(appCompatActivity.getApplicationContext()).build())
                .build();

    }

    public void start2() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {

        gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(context).build())
                .build();

    }

    @Override
    public void onResponse(Call<Update> call, Response<Update> response) {
        if(response.isSuccessful()) {
            Update customOrderMasterlist = response.body();
            if(context != null){
                if(faveAdapter != null)
                    faveAdapter.response(customOrderMasterlist.getData(), response.message(), customOrderMasterlist.getStatusCode());
                else if (orderDetailProductsAdapter != null)
                    orderDetailProductsAdapter.response(customOrderMasterlist.getData(), response.message(), customOrderMasterlist.getStatusCode());
            } else if(appCompatActivity instanceof PaymentAndShippingActvity) {
                ((PaymentAndShippingActvity) appCompatActivity).response(customOrderMasterlist.getData(), response.message(), customOrderMasterlist.getStatusCode());
            } else if (appCompatActivity instanceof ClaimActivity) {
                ((ClaimActivity) appCompatActivity).response(customOrderMasterlist.getData(), response.message(), customOrderMasterlist.getStatusCode());
            } else if (appCompatActivity instanceof InProgressActivity) {
                ((InProgressActivity) appCompatActivity).response(customOrderMasterlist.getData(), response.message(), customOrderMasterlist.getStatusCode());
            } else if (appCompatActivity instanceof FeedbackActivity) {
                ((FeedbackActivity) appCompatActivity).response(customOrderMasterlist.getData(), response.message(), customOrderMasterlist.getStatusCode());
            } else if (appCompatActivity instanceof UpdateUserActivity) {
                ((UpdateUserActivity) appCompatActivity).response(customOrderMasterlist.getData(), response.message(), customOrderMasterlist.getStatusCode());
            } else if (appCompatActivity instanceof OrderActivity) {
                ((OrderActivity) appCompatActivity).response(customOrderMasterlist.getData(), response.message(), customOrderMasterlist.getStatusCode());
            }
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<Update> call, Throwable t) {
        if(context != null) {
            if(faveAdapter != null)
                faveAdapter.fail(t);
            else if (orderDetailProductsAdapter != null)
                orderDetailProductsAdapter.fail(t);
        }else if(appCompatActivity instanceof PaymentAndShippingActvity) {
            ((PaymentAndShippingActvity) appCompatActivity).fail(t);
        } else if (appCompatActivity instanceof ClaimActivity) {
            ((ClaimActivity) appCompatActivity).fail(t);
        } else if (appCompatActivity instanceof InProgressActivity) {
            ((InProgressActivity) appCompatActivity).fail(t);
        } else if (appCompatActivity instanceof FeedbackActivity) {
            ((FeedbackActivity) appCompatActivity).fail(t);
        } else if (appCompatActivity instanceof UpdateUserActivity) {
            ((UpdateUserActivity) appCompatActivity).fail(t);
        } else if (appCompatActivity instanceof OrderActivity) {
            ((OrderActivity) appCompatActivity).fail(t);
        }
    }

    public void update(OrderMasterlist.Datum datum) {

        OrderMasterlistApi orderMasterlistApi = retrofit.create(OrderMasterlistApi.class);

        Call<Update> call = orderMasterlistApi.updateOrderMasterlistRunner(datum.getOrMasID(), datum);
        call.enqueue(this);
    }

    public void delete(Integer id) {

        FavoriteApi favorite = retrofit.create(FavoriteApi.class);
        Call<Update> call = favorite.removeFavorite(id);
        call.enqueue(this);
    }

    public void updateUser(Integer id, User.Data user) {

        UserApi userApi = retrofit.create(UserApi.class);
        Call<Update> call = userApi.updateUser(id, user);
        call.enqueue(this);
    }

    public void updateItemReturned(Integer id, OrderDetail orderDetail) {

        OrderDetailApi orderDetailApi = retrofit.create(OrderDetailApi.class);
        Call<Update> call = orderDetailApi.updateOrderDetail(id, orderDetail);
        call.enqueue(this);
    }
}


