package com.prototype.kompra.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.prototype.kompra.R;
import com.prototype.kompra.controller.UserController;
import com.prototype.kompra.model.User;
import com.prototype.kompra.util.CallbackTemplateSingleObject;
import com.prototype.kompra.util.Session;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class RunnerLoginActivity extends AppCompatActivity implements CallbackTemplateSingleObject {

    EditText email, password;
    Button login;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.runner_activity_login);
        getSupportActionBar().setTitle("Runner access");
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserController userController = new UserController(RunnerLoginActivity.this);
                try {
                    userController.start();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                }
                User.Data data = new User().new Data();
                data.setEmail(email.getText().toString());
                data.setPassword(password.getText().toString());
                userController.login(data);
            }
        });
    }

    @Override
    public void response(Object result, String message, Integer statusCode) {
        User user = (User) result;

        if (user.getData().getUserType().equals("employee")) {
            Session session = new Session(this);
            session.putId(this.getString(R.string.id), user.getData().getId());
            session.putToken(this.getString(R.string.token), user.getSuccess().getToken());
            session.putUser(this.getString(R.string.user), user);

            Intent i = new Intent(this, RunnerHomeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        } else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(RunnerLoginActivity.this);
            builder1.setMessage("Invalid user!");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }

    @Override
    public void fail(Throwable stackTrace) {

    }
}
