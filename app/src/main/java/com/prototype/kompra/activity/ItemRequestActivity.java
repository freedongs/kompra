package com.prototype.kompra.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.prototype.kompra.R;
import com.prototype.kompra.controller.CustomRequestController;
import com.prototype.kompra.controller.RequestController;
import com.prototype.kompra.model.RequestDetail;
import com.prototype.kompra.util.CallbackTemplateSingleObject;
import com.prototype.kompra.util.Session;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class ItemRequestActivity extends AppCompatActivity implements CallbackTemplateSingleObject {

    EditText request_product_name, request_product_desc, request_product_price, request_product_quantity;
    Button request;
    CustomRequestController customRequestController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_request);
        getSupportActionBar().setTitle("Request Item");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        request_product_name = findViewById(R.id.request_product_name);
        request_product_desc = findViewById(R.id.request_product_desc);
        request_product_price = findViewById(R.id.request_product_price);
        request_product_quantity = findViewById(R.id.request_product_quantity);
        request = findViewById(R.id.request);

        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRequestController = new CustomRequestController(ItemRequestActivity.this);
                Session session = new Session(ItemRequestActivity.this);
                RequestDetail.Datum requestDetail = new RequestDetail().new Datum();
                requestDetail.setItemReqName(request_product_name.getText().toString());
                requestDetail.setItemReqDesc(request_product_desc.getText().toString());
                requestDetail.setItemReqPrice(Double.parseDouble(request_product_price.getText().toString()));
                requestDetail.setItemReqQuantity(Integer.parseInt(request_product_quantity.getText().toString()));
                requestDetail.setApprove(0);
                requestDetail.setBUYERID(session.getId(v.getContext().getString(R.string.id)));
                try {
                    customRequestController.start();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                customRequestController.addRequest(requestDetail);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(ItemRequestActivity.this, HomePageActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
        return true;
    }


    @Override
    public void response(Object result, String message, Integer statusCode) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(ItemRequestActivity.this);
        builder1.setMessage("Request has been successfully created!");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

        alert11.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Intent i = new Intent(ItemRequestActivity.this, HomePageActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }

    @Override
    public void fail(Throwable stackTrace) {

    }
}
