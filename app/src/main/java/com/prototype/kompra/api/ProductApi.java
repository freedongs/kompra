package com.prototype.kompra.api;

import com.prototype.kompra.model.Product;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ProductApi {

    @GET("products")
    Call<Product> getAllProduct();

    @GET("products/{id}")
    Call<Product> getProduct(@Path("id") int id);

    @POST("products")
    Call<Product> addProduct(@Body Product product);

    @POST("products-delete/{id}")
    Call<Product> removeProduct(@Path("id") int id);

    @POST("products/{id}")
    Call<Product> updateProduct(@Path("id") int id, @Body Product product);

    @POST("products-subtract/{id}")
    Call<Product> subtractProduct(@Path("id") int id, @Body Product.Datum product);
}
