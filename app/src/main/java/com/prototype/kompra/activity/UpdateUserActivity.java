package com.prototype.kompra.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.prototype.kompra.R;
import com.prototype.kompra.controller.ResponseUpdateController;
import com.prototype.kompra.model.User;
import com.prototype.kompra.util.CallbackTemplateSingleObject;
import com.prototype.kompra.util.Session;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class UpdateUserActivity extends AppCompatActivity implements CallbackTemplateSingleObject {

    EditText firstName, lastName, address, occupation, gender, age, dob, email, password, confirm_password, license_number, contact_number;
    Button update;
    Calendar myCalendar = Calendar.getInstance();
    User data = new User();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_update);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        address = (EditText) findViewById(R.id.address);
        occupation = (EditText) findViewById(R.id.occupation);
        gender = (EditText) findViewById(R.id.gender);
        age = (EditText) findViewById(R.id.age);
        dob = (EditText) findViewById(R.id.dob);
        contact_number = (EditText) findViewById(R.id.contact_number);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        confirm_password = (EditText) findViewById(R.id.confirm_password);
        license_number = (EditText) findViewById(R.id.license_number);
        update = (Button) findViewById(R.id.update);
        Session session = new Session(this);
        data = session.getUser(this.getString(R.string.user));

        firstName.setText(data.getData().getBuyer().getBuyFname());
        lastName.setText(data.getData().getBuyer().getBuyLname());
        address.setText(data.getData().getBuyer().getBuyAdd());
        occupation.setText(data.getData().getBuyer().getBuyOccu());
        gender.setText(data.getData().getBuyer().getBuyGender());
        age.setText(data.getData().getBuyer().getBuyAge().toString());
        dob.setText(data.getData().getBuyer().getBuyDoB());
        contact_number.setText(data.getData().getBuyer().getBuyConNum());
        email.setText(data.getData().getEmail());
        password.setText(data.getData().getPassword());
        confirm_password.setText(data.getData().getC_password());

        getSupportActionBar().setTitle("Update");

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.user_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(v.getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResponseUpdateController responseUpdateController = new ResponseUpdateController(UpdateUserActivity.this);
                try {
                    responseUpdateController.start();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                }
                data.getData().setFirstName(firstName.getText().toString());
                data.getData().setLastName(lastName.getText().toString());
                data.getData().setName(firstName.getText().toString()+" "+lastName.getText().toString());
                data.getData().setAddress(address.getText().toString());
                data.getData().setOccupation(occupation.getText().toString());
                data.getData().setGender(gender.getText().toString());
                data.getData().setAge(Integer.parseInt(age.getText().toString()));
                data.getData().setDob(dob.getText().toString());
                data.getData().setEmail(email.getText().toString());
                data.getData().setPassword(password.getText().toString());
                data.getData().setC_password(confirm_password.getText().toString());
                data.getData().setBuyConNum(contact_number.getText().toString());
                data.getData().setUserType("buyer");

                if(firstName.getText().toString().trim().length() == 0){
                    firstName.setError("Field must not be empty");
                    return;
                }
                if(lastName.getText().toString().trim().length() == 0){
                    lastName.setError("Field must not be empty");
                    return;
                }
                if(address.getText().toString().trim().length() == 0){
                    address.setError("Field must not be empty");
                    return;
                }
                if(occupation.getText().toString().trim().length() == 0){
                    occupation.setError("Field must not be empty");
                    return;
                }
                if(gender.getText().toString().trim().length() == 0){
                    gender.setError("Field must not be empty");
                    return;
                }
                if(age.getText().toString().trim().length() == 0){
                    age.setError("Field must not be empty");
                    return;
                }
                if(dob.getText().toString().trim().length() == 0){
                    dob.setError("Field must not be empty");
                    return;
                }
                if(email.getText().toString().trim().length() == 0){
                    email.setError("Field must not be empty");
                    return;
                }
                if(password.getText().toString().trim().length() == 0){
                    password.setError("Field must not be empty");
                    return;
                }
                if(contact_number.getText().toString().trim().length() == 0){
                    contact_number.setError("Field must not be empty");
                    return;
                }

                responseUpdateController.updateUser(data.getData().getId(), data.getData());
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(UpdateUserActivity.this, HomePageActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();

        return true;
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dob.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void response(Object result, String message, Integer statusCode) {

        Session session = new Session(this);
        session.clearSession(this.getString(R.string.user));

        AlertDialog.Builder builder1 = new AlertDialog.Builder(UpdateUserActivity.this);
        builder1.setMessage("Succesfully update user!");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

        alert11.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Intent i = new Intent(UpdateUserActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }

    @Override
    public void fail(Throwable stackTrace) {
        System.out.println(stackTrace.getMessage());
    }
}
