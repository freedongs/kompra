package com.prototype.kompra.api;

import com.prototype.kompra.model.Category;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CategoryApi {

    @GET("category")
    Call<Category> getAllCategory();

    @GET("category/{id}")
    Call<Category> getCategory(@Path("id") int id);

    @GET("category-products/")
    Call<Category> getAllCategoryProduct();

    @GET("category-products/{id}")
    Call<Category> getCategoryProduct(@Path("id") int id);

    @POST("category")
    Call<Category> addCategory(@Body Category category);

    @POST("category-delete/{id}")
    Call<Category> removeCategory(@Path("id") int id);

    @POST("category/{id}")
    Call<Category> updateCategory(@Path("id") int id, @Body Category category);
}
