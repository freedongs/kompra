package com.prototype.kompra.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.prototype.kompra.R;
import com.prototype.kompra.activity.LoginActivity;
import com.prototype.kompra.activity.RegistrationActivity;
import com.prototype.kompra.controller.CustomFavoriteController;
import com.prototype.kompra.model.Favorite;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.util.App;
import com.prototype.kompra.util.CallbackTemplateSingleObject;
import com.prototype.kompra.util.Session;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends BaseAdapter implements CallbackTemplateSingleObject {

    List<Product.Datum> list;
    Context context;

    public ProductAdapter(List<Product.Datum> list, Context context)
    {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getCategoryId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_product_recycler_view, null);
        final Product.Datum datum = list.get(position);
        ImageView productPic = (ImageView) view.findViewById(R.id.product_image);
        TextView productName =(TextView) view.findViewById(R.id.product_name);
        TextView stock_status =(TextView) view.findViewById(R.id.stock_status);
        ImageView favorite =(ImageView) view.findViewById(R.id.favorite);
        if(datum.getProdQuantity() == 0) {
            stock_status.setVisibility(View.VISIBLE);
        }

        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(v.getContext());
                builder1.setMessage("Do you want to add this product in favorites?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface notify, int id) {
                                CustomFavoriteController customFavoriteController = new CustomFavoriteController(ProductAdapter.this, context);
                                Favorite.Data2 data2 = new Favorite.Data2();
                                Session session = new Session(context);
                                data2.setBUYERID(session.getId(context.getString(R.string.id)));
                                data2.setProdID(datum.getProdID());
                                try {
                                    customFavoriteController.start(data2);
                                } catch (CertificateException e) {
                                    e.printStackTrace();
                                } catch (NoSuchAlgorithmException e) {
                                    e.printStackTrace();
                                } catch (KeyStoreException e) {
                                    e.printStackTrace();
                                } catch (KeyManagementException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                notify.cancel();
                                Toast.makeText(v.getContext(), "Product is added as favorite", Toast.LENGTH_LONG).show();
                            }
                        });

                builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        productName.setText(datum.getProdName());
        if(datum.getProdPhoto() != null)
            Picasso.get().load(App.image_url + datum.getFile().getImageUrl()).placeholder(R.drawable.loading_animation).fit().into(productPic);
        else
            productPic.setImageResource(R.drawable.default_product);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(v.getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCanceledOnTouchOutside(true);

                View view = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_add_product, null);

                TextView productName = view.findViewById(R.id.product_name);
                TextView productDesc = view.findViewById(R.id.product_description);
                TextView productPrize = view.findViewById(R.id.product_prize);
                TextView product_category = view.findViewById(R.id.product_category);
                TextView product_measurement = view.findViewById(R.id.product_measurement);
                TextView product_stocks = view.findViewById(R.id.product_stocks);
                final EditText quantity = view.findViewById(R.id.product_quantity);
                ImageView productPic = view.findViewById(R.id.image);
                Button add = view.findViewById(R.id.add_product);

                productName.setText(datum.getProdName());
                productDesc.setText(datum.getProdDesc());
                productPrize.setText(datum.getProdPrice().toString());
                product_category.setText(datum.getCategory().getCategoryName());
                product_measurement.setText(datum.getProdMeasurement());
                product_stocks.setText(datum.getProdQuantity().toString());
                if(datum.getProdQuantity() == 0) {
                    add.setVisibility(View.GONE);
                    product_stocks.setText("No stocks available");
                    quantity.setEnabled(false);
                }
                if(datum.getProdPhoto() != null)
                    Picasso.get().load(App.image_url + datum.getFile().getImageUrl()).placeholder(R.drawable.loading_animation).fit().into(productPic);
                else
                    productPic.setImageResource(R.drawable.default_product);

                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(quantity.getText().toString().trim().length() == 0){
                            quantity.setError("Field must not be empty");
                            return;
                        }

                        if(Integer.parseInt(quantity.getText().toString()) > datum.getProdQuantity() || Integer.parseInt(quantity.getText().toString()) == 0){
                            quantity.setError("Stocks available is only: "+datum.getProdQuantity());
                            return;
                        }

                        Session session = new Session(v.getContext());
                        ArrayList<Product.Datum> data;

                        if (session.getSharedPreferences().contains(v.getContext().getString(R.string.cart))) {
                             data = new ArrayList<>(session.getProducts(v.getContext().getString(R.string.cart)));
                        } else {
                            data = new ArrayList<>();
                        }

                        Product.Datum temp = datum;
                        temp.setProdQuantity(Integer.parseInt(quantity.getText().toString()));
                        data.add(temp);
                        session.addCart(v.getContext().getString(R.string.cart), data);

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(v.getContext());
                        builder1.setMessage("Product added to the cart!");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface notify, int id) {
                                        notify.cancel();
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                });

                dialog.setContentView(view);

                dialog.show();
            }
        });
        return view;
    }

    @Override
    public void response(Object result, String message, Integer statusCode) {

    }

    @Override
    public void fail(Throwable stackTrace) {

    }
}