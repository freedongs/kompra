package com.prototype.kompra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Product {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Datum implements Serializable {

        @SerializedName("ProdID")
        @Expose
        private Integer prodID;
        @SerializedName("ProdName")
        @Expose
        private String prodName;
        @SerializedName("ProdDesc")
        @Expose
        private String prodDesc;
        @SerializedName("ProdPrice")
        @Expose
        private Double prodPrice;
        @SerializedName("ProdQuantity")
        @Expose
        private Integer prodQuantity;
        @SerializedName("ProdMeasurement")
        @Expose
        private String prodMeasurement;
        @SerializedName("ProdPhoto")
        @Expose
        private String prodPhoto;
        @SerializedName("category_id")
        @Expose
        private Integer categoryId;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("category")
        @Expose
        private Category.Datum category;
        @SerializedName("file")
        @Expose
        private File file;

        public File getFile() {
            return file;
        }

        public void setFile(File file) {
            this.file = file;
        }

        public Category.Datum getCategory() {
            return category;
        }

        public void setCategory(Category.Datum category) {
            this.category = category;
        }

        public Integer getProdID() {
            return prodID;
        }

        public void setProdID(Integer prodID) {
            this.prodID = prodID;
        }

        public String getProdName() {
            return prodName;
        }

        public void setProdName(String prodName) {
            this.prodName = prodName;
        }

        public String getProdDesc() {
            return prodDesc;
        }

        public void setProdDesc(String prodDesc) {
            this.prodDesc = prodDesc;
        }

        public Double getProdPrice() {
            return prodPrice;
        }

        public void setProdPrice(Double prodPrice) {
            this.prodPrice = prodPrice;
        }

        public Integer getProdQuantity() {
            return prodQuantity;
        }

        public void setProdQuantity(Integer prodQuantity) {
            this.prodQuantity = prodQuantity;
        }

        public String getProdMeasurement() {
            return prodMeasurement;
        }

        public void setProdMeasurement(String prodMeasurement) {
            this.prodMeasurement = prodMeasurement;
        }

        public String getProdPhoto() {
            return prodPhoto;
        }

        public void setProdPhoto(String prodPhoto) {
            this.prodPhoto = prodPhoto;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }
}
