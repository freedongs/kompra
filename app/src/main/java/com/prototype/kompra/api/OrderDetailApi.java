package com.prototype.kompra.api;

import com.prototype.kompra.model.OrderDetail;
import com.prototype.kompra.model.Update;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface OrderDetailApi {

    @GET("orderdetails")
    Call<OrderDetail> getAllOrderDetail();

    @GET("orderdetails/{id}")
    Call<OrderDetail> getOrderDetail(@Path("id") int id);

    @POST("orderdetails")
    Call<OrderDetail> addOrderDetail(@Body OrderDetail.Datum orderDetail);

    @POST("orderdetails-delete/{id}")
    Call<OrderDetail> removeOrderDetail(@Path("id") int id);

    @POST("orderdetails/{id}")
    Call<Update> updateOrderDetail(@Path("id") int id, @Body OrderDetail orderDetail);
}
