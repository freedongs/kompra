package com.prototype.kompra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Favorite implements Serializable{

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Data implements Serializable {
        @SerializedName("BUYERID")
        @Expose
        private Integer bUYERID;
        @SerializedName("BuyFname")
        @Expose
        private String buyFname;
        @SerializedName("BuyMI")
        @Expose
        private String buyMI;
        @SerializedName("BuyLname")
        @Expose
        private String buyLname;
        @SerializedName("BuyAge")
        @Expose
        private Integer buyAge;
        @SerializedName("BuyConNum")
        @Expose
        private String buyConNum;
        @SerializedName("BuyGender")
        @Expose
        private String buyGender;
        @SerializedName("BuyOccu")
        @Expose
        private String buyOccu;
        @SerializedName("BuyAdd")
        @Expose
        private String buyAdd;
        @SerializedName("BuyDoB")
        @Expose
        private String buyDoB;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("favorite")
        @Expose
        private List<Favorite.Data2> favorite = null;

        public Integer getBUYERID() {
            return bUYERID;
        }

        public void setBUYERID(Integer bUYERID) {
            this.bUYERID = bUYERID;
        }

        public String getBuyFname() {
            return buyFname;
        }

        public void setBuyFname(String buyFname) {
            this.buyFname = buyFname;
        }

        public String getBuyMI() {
            return buyMI;
        }

        public void setBuyMI(String buyMI) {
            this.buyMI = buyMI;
        }

        public String getBuyLname() {
            return buyLname;
        }

        public void setBuyLname(String buyLname) {
            this.buyLname = buyLname;
        }

        public Integer getBuyAge() {
            return buyAge;
        }

        public void setBuyAge(Integer buyAge) {
            this.buyAge = buyAge;
        }

        public String getBuyConNum() {
            return buyConNum;
        }

        public void setBuyConNum(String buyConNum) {
            this.buyConNum = buyConNum;
        }

        public String getBuyGender() {
            return buyGender;
        }

        public void setBuyGender(String buyGender) {
            this.buyGender = buyGender;
        }

        public String getBuyOccu() {
            return buyOccu;
        }

        public void setBuyOccu(String buyOccu) {
            this.buyOccu = buyOccu;
        }

        public String getBuyAdd() {
            return buyAdd;
        }

        public void setBuyAdd(String buyAdd) {
            this.buyAdd = buyAdd;
        }

        public String getBuyDoB() {
            return buyDoB;
        }

        public void setBuyDoB(String buyDoB) {
            this.buyDoB = buyDoB;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<Favorite.Data2> getFavorite() {
            return favorite;
        }

        public void setFavorite(List<Favorite.Data2> favorite) {
            this.favorite = favorite;
        }
    }

    public static class Data2 implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("BUYERID")
        @Expose
        private Integer bUYERID;
        @SerializedName("ProdID")
        @Expose
        private Integer prodID;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("product")
        @Expose
        private Product.Datum product;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getBUYERID() {
            return bUYERID;
        }

        public void setBUYERID(Integer bUYERID) {
            this.bUYERID = bUYERID;
        }

        public Integer getProdID() {
            return prodID;
        }

        public void setProdID(Integer prodID) {
            this.prodID = prodID;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Product.Datum getProduct() {
            return product;
        }

        public void setProduct(Product.Datum product) {
            this.product = product;
        }
    }

}
