package com.prototype.kompra.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;

import com.prototype.kompra.R;
import com.prototype.kompra.adapter.ProductAdapter;
import com.prototype.kompra.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductsActivity extends AppCompatActivity {

    GridView gridView;
    ProductAdapter productAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_page);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Products");
        gridView = findViewById(R.id.products_grid_view);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("container");
        List<Product.Datum> data = (ArrayList<Product.Datum>) args.getSerializable("products");

        productAdapter = new ProductAdapter(data, this);
        gridView.setAdapter(productAdapter);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(ProductsActivity.this, HomePageActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
        return true;
    }

}
