package com.prototype.kompra.util;

import java.util.List;

public interface CallbackResponseTemplate<T> {

    void response(List<Object> result, String message, Integer statusCode);
    void fail(Throwable stackTrace);
}
