package com.prototype.kompra.controller;

import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.activity.ItemRequestActivity;
import com.prototype.kompra.api.RequestDetailApi;
import com.prototype.kompra.model.CustomRequestDetail;
import com.prototype.kompra.model.RequestDetail;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CustomRequestController implements Callback<CustomRequestDetail> {

    AppCompatActivity appCompatActivity;
    Gson gson;
    Retrofit retrofit;

    public CustomRequestController(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    public void start() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(appCompatActivity).build())
                .build();
    }

    @Override
    public void onResponse(Call<CustomRequestDetail> call, Response<CustomRequestDetail> response) {
        if(response.isSuccessful()) {
            CustomRequestDetail customRequestDetail = response.body();
                ((ItemRequestActivity) appCompatActivity).response(customRequestDetail.getData(), response.message(), customRequestDetail.getStatusCode());
        } else {
            System.out.println(response.errorBody());
        }
    }


    public void addRequest(RequestDetail.Datum requestDetail) {
        RequestDetailApi requestDetailApi = retrofit.create(RequestDetailApi.class);

        Call<CustomRequestDetail> call = requestDetailApi.addRequestDetail(requestDetail);
        call.enqueue(this);
    }

    @Override
    public void onFailure(Call<CustomRequestDetail> call, Throwable t) {
        ((ItemRequestActivity) appCompatActivity).fail(t);
    }
}
