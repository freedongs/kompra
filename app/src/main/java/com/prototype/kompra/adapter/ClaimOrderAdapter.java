package com.prototype.kompra.adapter;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.prototype.kompra.R;
import com.prototype.kompra.model.OrderDetail;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.util.App;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ClaimOrderAdapter extends RecyclerView.Adapter<ClaimOrderAdapter.ProductViewHolder> {

    List<OrderDetail> list;

    public ClaimOrderAdapter(List<OrderDetail> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_order_product_recycler_view, viewGroup, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder productViewHolder, int i) {
        OrderDetail orderDetail = list.get(i);
        productViewHolder.productName.setText(orderDetail.getProducts().getProdName());
        productViewHolder.productDescription.setText(orderDetail.getProducts().getProdDesc());
        productViewHolder.productPrize.setText(orderDetail.getProducts().getProdPrice().toString());
        productViewHolder.productQuantity.setText(orderDetail.getOrDetCount().toString());
        if(orderDetail.getProducts().getProdPhoto() != null)
            Picasso.get().load(App.image_url + orderDetail.getProducts().getFile().getImageUrl()).placeholder(R.drawable.loading_animation).fit().into(productViewHolder.productPic);
        else
            productViewHolder.productPic.setImageResource(R.drawable.default_product);
        productViewHolder.datum = orderDetail.getProducts();
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView productPic;
        public TextView productName;
        public TextView productDescription;
        public TextView productPrize;
        public TextView productQuantity;
        public Product.Datum datum;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);

            productPic = (ImageView) itemView.findViewById(R.id.product_image);
            productName = (TextView) itemView.findViewById(R.id.product_name);
            productDescription = (TextView) itemView.findViewById(R.id.product_description);
            productPrize = (TextView) itemView.findViewById(R.id.product_prize);
            productQuantity = (TextView) itemView.findViewById(R.id.product_quantity);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Dialog dialog = new Dialog(v.getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(true);

            View view = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_view_product, null);

            TextView productName = view.findViewById(R.id.product_name);
            TextView productDesc = view.findViewById(R.id.product_description);
            TextView productPrize = view.findViewById(R.id.product_prize);
            TextView productQuantity = view.findViewById(R.id.product_quantity);

            productName.setText(datum.getProdName());
            productDesc.setText(datum.getProdDesc());
            productPrize.setText(datum.getProdPrice().toString());
            productQuantity.setText(datum.getProdQuantity().toString());
            if(datum.getProdPhoto() != null)
                Picasso.get().load(App.image_url + datum.getFile().getImageUrl()).placeholder(R.drawable.loading_animation).fit().into(productPic);
            else
                productPic.setImageResource(R.drawable.default_product);
        }
    }
}
