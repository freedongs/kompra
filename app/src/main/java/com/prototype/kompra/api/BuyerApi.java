package com.prototype.kompra.api;

import com.prototype.kompra.model.Buyer;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface BuyerApi {

    @GET("buyers")
    Call<Buyer> getAllBuyer();

    @GET("buyers/{id}")
    Call<Buyer> getBuyer(@Path("id") int id);

    @POST("buyers")
    Call<Buyer> addBuyer(@Body Buyer buyer);

    @POST("buyers-delete/{id}")
    Call<Buyer> removeBuyer(@Path("id") int id);

    @POST("buyers/{id}")
    Call<Buyer> updateBuyer(@Path("id") int id, @Body Buyer buyer);
}
