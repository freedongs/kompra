package com.prototype.kompra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Search implements Serializable {

    @SerializedName("term")
    @Expose
    private String term;
    @SerializedName("is_category")
    @Expose
    private Boolean is_category;

    public Search(String term, Boolean is_category) {
        this.term = term;
        this.is_category = is_category;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public Boolean getIs_category() {
        return is_category;
    }

    public void setIs_category(Boolean is_category) {
        this.is_category = is_category;
    }
}
