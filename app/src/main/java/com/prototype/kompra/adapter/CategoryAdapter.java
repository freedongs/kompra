package com.prototype.kompra.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.prototype.kompra.R;
import com.prototype.kompra.activity.ProductsActivity;
import com.prototype.kompra.model.Category;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.util.App;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>{

    List<Category.Datum> categoryList;

    public CategoryAdapter(List<Category.Datum> categoryList)
    {
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_category_recycler_view, viewGroup, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return this.categoryList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder categoryViewHolder, int i) {
        Category.Datum category = categoryList.get(i);
        categoryViewHolder.categoryName.setText(category.getCategoryName());
        if(category.getCategoryPhoto() != null)
            Picasso.get().load(App.image_url + category.getFile().getImageUrl()).placeholder(R.drawable.loading_animation).fit().into(categoryViewHolder.categoryPic);
        else
            categoryViewHolder.categoryPic.setImageResource(R.drawable.default_category);
        categoryViewHolder.list = category.getProduct();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView categoryPic;
        public TextView categoryName;
        public List<Product.Datum> list;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);

            categoryPic = (ImageView) itemView.findViewById(R.id.category_image);
            categoryName = (TextView) itemView.findViewById(R.id.category_name);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(v.getContext(), ProductsActivity.class);
            Bundle args = new Bundle();
            args.putSerializable("products", (Serializable) list);
            intent.putExtra("container", args);
            v.getContext().startActivity(intent);
        }
    }
}
