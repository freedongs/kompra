package com.prototype.kompra.util;

public interface CallbackTemplateSingleObject {

    void response(Object result, String message, Integer statusCode);
    void fail(Throwable stackTrace);
}
