package com.prototype.kompra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OrderMasterlist implements Serializable {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public static class Datum implements Serializable{

        @SerializedName("OrMasID")
        @Expose
        private Integer orMasID;
        @SerializedName("TotAmount")
        @Expose
        private Double totAmount;
        @SerializedName("PaymentType")
        @Expose
        private String paymentType;
        @SerializedName("DeliveryDate")
        @Expose
        private String deliveryDate;
        @SerializedName("ShippingAdd")
        @Expose
        private String shippingAdd;
        @SerializedName("OrDetID")
        @Expose
        private Integer orDetID;
        @SerializedName("BUYERID")
        @Expose
        private Integer bUYERID;
        @SerializedName("FEEDRATEID")
        @Expose
        private Integer fEEDRATEID;
        @SerializedName("OrTracID")
        @Expose
        private Integer orTracID;
        @SerializedName("EMPID")
        @Expose
        private Integer eMPID;
        @SerializedName("ReqDetID")
        @Expose
        private Integer reqDetID;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("order_details")
        @Expose
        private List<OrderDetail> orderDetails = null;
        @SerializedName("buyer")
        @Expose
        private Buyer buyer;
        @SerializedName("feedback_rating")
        @Expose
        private FeedbackAndRate.Data feedbackRate;
        @SerializedName("employee")
        @Expose
        private Employee employee;
        @SerializedName("order_tracking_status")
        @Expose
        private OrderTrackingStatus orderTrackingStatus;

        public Integer getOrMasID() {
            return orMasID;
        }

        public void setOrMasID(Integer orMasID) {
            this.orMasID = orMasID;
        }

        public Double getTotAmount() {
            return totAmount;
        }

        public void setTotAmount(Double totAmount) {
            this.totAmount = totAmount;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getDeliveryDate() {
            return deliveryDate;
        }

        public void setDeliveryDate(String deliveryDate) {
            this.deliveryDate = deliveryDate;
        }

        public String getShippingAdd() {
            return shippingAdd;
        }

        public void setShippingAdd(String shippingAdd) {
            this.shippingAdd = shippingAdd;
        }

        public Integer getOrDetID() {
            return orDetID;
        }

        public void setOrDetID(Integer orDetID) {
            this.orDetID = orDetID;
        }

        public Integer getBUYERID() {
            return bUYERID;
        }

        public void setBUYERID(Integer bUYERID) {
            this.bUYERID = bUYERID;
        }

        public Integer getFEEDRATEID() {
            return fEEDRATEID;
        }

        public void setFEEDRATEID(Integer fEEDRATEID) {
            this.fEEDRATEID = fEEDRATEID;
        }

        public Integer getOrTracID() {
            return orTracID;
        }

        public void setOrTracID(Integer orTracID) {
            this.orTracID = orTracID;
        }

        public Integer getEMPID() {
            return eMPID;
        }

        public void setEMPID(Integer eMPID) {
            this.eMPID = eMPID;
        }

        public Integer getReqDetID() {
            return reqDetID;
        }

        public void setReqDetID(Integer reqDetID) {
            this.reqDetID = reqDetID;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<OrderDetail> getOrderDetails() {
            return orderDetails;
        }

        public void setOrderDetails(List<OrderDetail> orderDetails) {
            this.orderDetails = orderDetails;
        }

        public Buyer getBuyer() {
            return buyer;
        }

        public void setBuyer(Buyer buyer) {
            this.buyer = buyer;
        }

        public FeedbackAndRate.Data getFeedbackRating() {
            return feedbackRate;
        }

        public void setFeedbackRating(FeedbackAndRate.Data feedbackRating) {
            this.feedbackRate = feedbackRating;
        }

        public Employee getEmployee() {
            return employee;
        }

        public void setEmployee(Employee employee) {
            this.employee = employee;
        }

        public OrderTrackingStatus getOrderTrackingStatus() {
            return orderTrackingStatus;
        }

        public void setOrderTrackingStatus(OrderTrackingStatus orderTrackingStatus) {
            this.orderTrackingStatus = orderTrackingStatus;
        }

    }
}
