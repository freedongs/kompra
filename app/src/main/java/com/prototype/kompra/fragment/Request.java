package com.prototype.kompra.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prototype.kompra.R;
import com.prototype.kompra.adapter.RequestAdapter;
import com.prototype.kompra.controller.RequestController;
import com.prototype.kompra.model.RequestDetail;
import com.prototype.kompra.util.CallbackResponseTemplate;
import com.prototype.kompra.util.Session;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;

public class Request extends Fragment implements CallbackResponseTemplate {

    RequestController requestController;
    RecyclerView recyclerView;
    RequestAdapter requestAdapter;

    @Override
    public void onResume() {
        super.onResume();
        requestController = new RequestController(this);
        try {
            requestController.start();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Session session = new Session(getActivity());
        requestController.displayRequest(session.getId(getActivity().getString(R.string.id)));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request, container, false);
        recyclerView = view.findViewById(R.id.request_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
    }

    @Override
    public void response(List result, String message, Integer statusCode) {
        recyclerView.setAdapter(null);
        requestAdapter = new RequestAdapter((List<RequestDetail.Datum>) result);
        requestAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(requestAdapter);
    }

    @Override
    public void fail(Throwable stackTrace) {

    }
}
