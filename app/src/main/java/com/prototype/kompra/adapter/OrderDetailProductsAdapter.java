package com.prototype.kompra.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prototype.kompra.R;
import com.prototype.kompra.activity.HomePageActivity;
import com.prototype.kompra.activity.LoginActivity;
import com.prototype.kompra.activity.RegistrationActivity;
import com.prototype.kompra.controller.ResponseUpdateController;
import com.prototype.kompra.controller.ReturnController;
import com.prototype.kompra.model.OrderDetail;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.model.Return;
import com.prototype.kompra.util.App;
import com.prototype.kompra.util.CallbackTemplateSingleObject;
import com.prototype.kompra.util.Session;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;

public class OrderDetailProductsAdapter extends RecyclerView.Adapter<OrderDetailProductsAdapter.ProductViewHolder> {

    List<OrderDetail> list;
    OrderMasterlist.Datum order_master;

    public OrderDetailProductsAdapter(List<OrderDetail> list, OrderMasterlist.Datum datum) {
        this.list = list;
        this.order_master = datum;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_order_product_recycler_view, viewGroup, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder productViewHolder, int i) {
        OrderDetail orderDetail = list.get(i);
        productViewHolder.productName.setText(orderDetail.getProducts().getProdName());
        productViewHolder.productDescription.setText(orderDetail.getProducts().getProdDesc());
        productViewHolder.productPrize.setText(orderDetail.getProducts().getProdPrice().toString());
        productViewHolder.productQuantity.setText(orderDetail.getOrDetCount().toString());
        if(orderDetail.getReturned() == 1)
            productViewHolder.txt_returned.setVisibility(View.VISIBLE);
        if(orderDetail.getProducts().getProdPhoto() != null)
            Picasso.get().load(App.image_url + orderDetail.getProducts().getFile().getImageUrl()).placeholder(R.drawable.loading_animation).fit().into(productViewHolder.productPic);
        else
            productViewHolder.productPic.setImageResource(R.drawable.default_product);
        productViewHolder.datum = orderDetail.getProducts();
        productViewHolder.orderDetail = orderDetail;
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, CallbackTemplateSingleObject {

        public ImageView productPic;
        public TextView productName;
        public TextView productDescription;
        public TextView productPrize;
        public TextView productQuantity;
        public LinearLayout txt_returned;
        public Product.Datum datum;
        public OrderDetail orderDetail;
        Dialog masterDialog;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);

            productPic = (ImageView) itemView.findViewById(R.id.product_image);
            productName = (TextView) itemView.findViewById(R.id.product_name);
            productDescription = (TextView) itemView.findViewById(R.id.product_description);
            productPrize = (TextView) itemView.findViewById(R.id.product_prize);
            productQuantity = (TextView) itemView.findViewById(R.id.product_quantity);
            txt_returned = (LinearLayout) itemView.findViewById(R.id.txt_returned);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            masterDialog = new Dialog(v.getContext());
            masterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            masterDialog.setCanceledOnTouchOutside(true);

            View view = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_view_with_action_product, null);

            TextView productName = view.findViewById(R.id.product_name);
            TextView productDesc = view.findViewById(R.id.product_description);
            TextView productPrize = view.findViewById(R.id.product_prize);
            TextView productQuantity = view.findViewById(R.id.product_quantity);
            LinearLayout txt_returned = view.findViewById(R.id.txt_returned);
            Button return_product = view.findViewById(R.id.return_product);

            return_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ResponseUpdateController responseUpdateController = new ResponseUpdateController(ProductViewHolder.this, v.getContext());
                    try {
                        responseUpdateController.start2();
                    } catch (CertificateException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (KeyStoreException e) {
                        e.printStackTrace();
                    } catch (KeyManagementException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    orderDetail.setProducts(null);
                    orderDetail.setReturned(1);
                    responseUpdateController.updateItemReturned(orderDetail.getOrDetID(), orderDetail);
                }
            });
            productName.setText(datum.getProdName());
            productDesc.setText(datum.getProdDesc());
            productPrize.setText(datum.getProdPrice().toString());
            productQuantity.setText(datum.getProdQuantity().toString());

            if(order_master.getOrTracID() == 4) {
                return_product.setVisibility(View.VISIBLE);
            }
            if(orderDetail.getReturned() == 1)
                txt_returned.setVisibility(View.VISIBLE);
            if(datum.getProdPhoto() != null)
                Picasso.get().load(App.image_url + datum.getFile().getImageUrl()).placeholder(R.drawable.loading_animation).fit().into(productPic);
            else
                productPic.setImageResource(R.drawable.default_product);

            masterDialog.setContentView(view);
            masterDialog.show();
        }

        @Override
        public void response(Object result, String message, Integer statusCode) {
            ReturnController returnController = new ReturnController(this, itemView.getContext());
            try {
                returnController.start();
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Return.Datum r = new Return().new Datum();
            r.setProdIDs(datum.getProdID());
            r.setProdQty(datum.getProdQuantity());
            Session session = new Session(itemView.getContext());
            r.setId(session.getId(itemView.getContext().getString(R.string.id)));
            r.setReceived(0);
            returnController.addReturn(r);

            AlertDialog.Builder builder1 = new AlertDialog.Builder(itemView.getContext());
            builder1.setMessage("Item is requested to return!.");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            masterDialog.dismiss();
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();

            alert11.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Intent i = new Intent(itemView.getContext(), HomePageActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    itemView.getContext().startActivity(i);
                }
            });
        }

        @Override
        public void fail(Throwable stackTrace) {
            System.out.println(stackTrace.getMessage());
        }
    }
}
