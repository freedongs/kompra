package com.prototype.kompra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Return implements Serializable {

    @SerializedName("data")
    @Expose
    private List<Return.Datum> data = null;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Return.Datum> getData() {
        return data;
    }

    public void setData(List<Return.Datum> data) {
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Datum implements Serializable{

        @SerializedName("ReturnID")
        @Expose
        private Integer returnId;
        @SerializedName("ProdIDs")
        @Expose
        private Integer prodIDs;
        @SerializedName("ProdQty")
        @Expose
        private Integer prodQty;
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("received")
        @Expose
        private Integer received;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getReceived() {
            return received;
        }

        public void setReceived(Integer received) {
            this.received = received;
        }

        public Integer getReturnId() {
            return returnId;
        }

        public void setReturnId(Integer returnId) {
            this.returnId = returnId;
        }

        public Integer getProdIDs() {
            return prodIDs;
        }

        public void setProdIDs(Integer prodIDs) {
            this.prodIDs = prodIDs;
        }

        public Integer getProdQty() {
            return prodQty;
        }

        public void setProdQty(Integer prodQty) {
            this.prodQty = prodQty;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }
}
