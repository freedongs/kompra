package com.prototype.kompra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OrderDetail implements Serializable {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @SerializedName("OrDetID")
    @Expose
    private Integer orDetID;
    @SerializedName("OrDetCount")
    @Expose
    private Integer orDetCount;
    @SerializedName("OrDetItemDiscount")
    @Expose
    private Integer orDetItemDiscount;
    @SerializedName("OrDetDate")
    @Expose
    private String orDetDate;
    @SerializedName("ProdID")
    @Expose
    private Integer prodID;
    @SerializedName("returned")
    @Expose
    private Integer returned;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("products")
    @Expose
    private Product.Datum products;

    public Integer getReturned() {
        return returned;
    }

    public void setReturned(Integer returned) {
        this.returned = returned;
    }

    public Integer getOrDetID() {
        return orDetID;
    }

    public void setOrDetID(Integer orDetID) {
        this.orDetID = orDetID;
    }

    public Integer getOrDetCount() {
        return orDetCount;
    }

    public void setOrDetCount(Integer orDetCount) {
        this.orDetCount = orDetCount;
    }

    public Integer getOrDetItemDiscount() {
        return orDetItemDiscount;
    }

    public void setOrDetItemDiscount(Integer orDetItemDiscount) {
        this.orDetItemDiscount = orDetItemDiscount;
    }

    public String getOrDetDate() {
        return orDetDate;
    }

    public void setOrDetDate(String orDetDate) {
        this.orDetDate = orDetDate;
    }

    public Integer getProdID() {
        return prodID;
    }

    public void setProdID(Integer prodID) {
        this.prodID = prodID;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Product.Datum getProducts() {
        return products;
    }

    public void setProducts(Product.Datum products) {
        this.products = products;
    }

    public class Datum implements Serializable{

        @SerializedName("OrDetID")
        @Expose
        private Integer orDetID;
        @SerializedName("OrDetCount")
        @Expose
        private Integer orDetCount;
        @SerializedName("OrDetItemDiscount")
        @Expose
        private Integer orDetItemDiscount;
        @SerializedName("OrDetDate")
        @Expose
        private String orDetDate;
        @SerializedName("ProdID")
        @Expose
        private Object prodID;
        @SerializedName("returned")
        @Expose
        private Integer returned;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("products")
        @Expose
        private Product.Datum products;

        public Integer getReturned() {
            return returned;
        }

        public void setReturned(Integer returned) {
            this.returned = returned;
        }

        public Integer getOrDetID() {
            return orDetID;
        }

        public void setOrDetID(Integer orDetID) {
            this.orDetID = orDetID;
        }

        public Integer getOrDetCount() {
            return orDetCount;
        }

        public void setOrDetCount(Integer orDetCount) {
            this.orDetCount = orDetCount;
        }

        public Integer getOrDetItemDiscount() {
            return orDetItemDiscount;
        }

        public void setOrDetItemDiscount(Integer orDetItemDiscount) {
            this.orDetItemDiscount = orDetItemDiscount;
        }

        public String getOrDetDate() {
            return orDetDate;
        }

        public void setOrDetDate(String orDetDate) {
            this.orDetDate = orDetDate;
        }

        public Object getProdID() {
            return prodID;
        }

        public void setProdID(Object prodID) {
            this.prodID = prodID;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Product.Datum getProducts() {
            return products;
        }

        public void setProducts(Product.Datum products) {
            this.products = products;
        }
    }
}
