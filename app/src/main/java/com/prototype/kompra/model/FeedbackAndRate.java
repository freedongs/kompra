package com.prototype.kompra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FeedbackAndRate implements Serializable {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @SerializedName("FEEDRATEID")
    @Expose
    private Integer fEEDRATEID;
    @SerializedName("ORDERRATE")
    @Expose
    private Integer oRDERRATE;
    @SerializedName("ORDERFEEDBACKS")
    @Expose
    private String oRDERFEEDBACKS;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getFEEDRATEID() {
        return fEEDRATEID;
    }

    public void setFEEDRATEID(Integer fEEDRATEID) {
        this.fEEDRATEID = fEEDRATEID;
    }

    public Integer getORDERRATE() {
        return oRDERRATE;
    }

    public void setORDERRATE(Integer oRDERRATE) {
        this.oRDERRATE = oRDERRATE;
    }

    public String getORDERFEEDBACKS() {
        return oRDERFEEDBACKS;
    }

    public void setORDERFEEDBACKS(String oRDERFEEDBACKS) {
        this.oRDERFEEDBACKS = oRDERFEEDBACKS;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public class Data implements Serializable {

        @SerializedName("FEEDRATEID")
        @Expose
        private Integer fEEDRATEID;
        @SerializedName("ORDERRATE")
        @Expose
        private Double oRDERRATE;
        @SerializedName("ORDERFEEDBACKS")
        @Expose
        private String oRDERFEEDBACKS;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getFEEDRATEID() {
            return fEEDRATEID;
        }

        public void setFEEDRATEID(Integer fEEDRATEID) {
            this.fEEDRATEID = fEEDRATEID;
        }

        public Double getORDERRATE() {
            return oRDERRATE;
        }

        public void setORDERRATE(Double oRDERRATE) {
            this.oRDERRATE = oRDERRATE;
        }

        public String getORDERFEEDBACKS() {
            return oRDERFEEDBACKS;
        }

        public void setORDERFEEDBACKS(String oRDERFEEDBACKS) {
            this.oRDERFEEDBACKS = oRDERFEEDBACKS;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }
}
