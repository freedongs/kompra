package com.prototype.kompra.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.activity.FeedbackActivity;
import com.prototype.kompra.api.FeedbackAndRateApi;
import com.prototype.kompra.model.FeedbackAndRate;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FeedbackController implements Callback<FeedbackAndRate> {

    FeedbackActivity feedbackActivity;

    public FeedbackController(FeedbackActivity feedbackActivity) {
        this.feedbackActivity = feedbackActivity;
    }

    public void start (FeedbackAndRate.Data feedbackAndRate) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(feedbackActivity).build())
                .build();

        FeedbackAndRateApi feedbackAndRateApi = retrofit.create(FeedbackAndRateApi.class);

        Call<FeedbackAndRate> call = feedbackAndRateApi.addFeedbackAndRate(feedbackAndRate);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<FeedbackAndRate> call, Response<FeedbackAndRate> response) {

        if(response.isSuccessful()) {
            FeedbackAndRate feedbackAndRate = response.body();
            feedbackActivity.response(feedbackAndRate.getData(), response.message(), feedbackAndRate.getStatusCode());
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<FeedbackAndRate> call, Throwable t) {
        feedbackActivity.fail(t);
    }
}
