package com.prototype.kompra.api;

import com.prototype.kompra.model.CustomRequestDetail;
import com.prototype.kompra.model.RequestDetail;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RequestDetailApi {

    @GET("requestdetails")
    Call<RequestDetail> getAllRequestDetail();

    @GET("requestdetails-request-buyer/{id}")
    Call<RequestDetail> getRequestDetailBuyer(@Path("id") int id);

    @GET("requestdetails/{id}")
    Call<RequestDetail> getRequestDetail(@Path("id") int id);

    @POST("requestdetails")
    Call<CustomRequestDetail> addRequestDetail(@Body RequestDetail.Datum requestDetail);

    @POST("requestdetails-delete/{id}")
    Call<RequestDetail> removeRequestDetail(@Path("id") int id);

    @POST("requestdetails/{id}")
    Call<RequestDetail> updateRequestDetail(@Path("id") int id, @Body RequestDetail requestDetail);
}
