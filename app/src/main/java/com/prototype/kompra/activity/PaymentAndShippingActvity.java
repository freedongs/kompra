package com.prototype.kompra.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.prototype.kompra.R;
import com.prototype.kompra.controller.CustomOrderMasterlistController;
import com.prototype.kompra.model.CustomOrderMasterlist;
import com.prototype.kompra.model.OrderDetail;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.model.Promo;
import com.prototype.kompra.model.User;
import com.prototype.kompra.util.CallbackTemplateSingleObject;
import com.prototype.kompra.util.Session;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PaymentAndShippingActvity extends AppCompatActivity implements CallbackTemplateSingleObject {

    List<Product.Datum> datum;
    List<Promo.Datum> promo;
    Double total = 0.0;
    Double overall_total = 0.0;
    Double pwd_total = 0.0;
    Double service_ch = 0.0;
    Double vat_ch = 0.0;
    EditText shipping_address, delivery_date, delivery_time;
    TextView cart_total, cart_total_pwd_discount, promo_code, initial_total, service_charge, vat_charge;
    Calendar myCalendar = Calendar.getInstance();
    LinearLayout layout_pwd;
    LinearLayout layout_promo_code;
    Session session;
    User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_and_shipping);
        cart_total = (TextView) findViewById(R.id.cart_total);
        promo_code = (TextView) findViewById(R.id.promo_code);
        cart_total_pwd_discount = (TextView) findViewById(R.id.cart_total_pwd_discount);
        initial_total = (TextView) findViewById(R.id.initial_total);
        service_charge = (TextView) findViewById(R.id.service_charge);
        vat_charge = (TextView) findViewById(R.id.vat_charge);
        final Spinner spinner = (Spinner) findViewById(R.id.spinner1);
        delivery_date = (EditText) findViewById(R.id.delivery_date);
        delivery_time = (EditText) findViewById(R.id.delivery_time);
        shipping_address = (EditText) findViewById(R.id.shipping_address);
        layout_pwd =  findViewById(R.id.layout_pwd);
        layout_promo_code =  findViewById(R.id.layout_promo_code);

        Button confirm = (Button) findViewById(R.id.confirm);

        getSupportActionBar().setTitle("Checkout");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        session = new Session(PaymentAndShippingActvity.this);
        user = session.getUser(this.getString(R.string.user));
        datum = new ArrayList<Product.Datum>((Collection<? extends Product.Datum>) getIntent().getSerializableExtra("products"));
        promo = new ArrayList<Promo.Datum>((Collection<? extends Promo.Datum>) getIntent().getSerializableExtra("promo"));

        for(Product.Datum datum : datum){
            overall_total = overall_total + (datum.getProdQuantity() * datum.getProdPrice());
        }

        total = overall_total;
        service_ch = total * 0.10;
        vat_ch = total * 0.12;
        total = total + service_ch + vat_ch;
        cart_total.setText(String.format("PHP %.2f", total));
        initial_total.setText(String.format("PHP %.2f", overall_total));
        service_charge.setText(String.format("PHP %.2f", service_ch));
        vat_charge.setText(String.format("PHP %.2f", vat_ch));

        if (promo.size() > 0) {
            layout_promo_code.setVisibility(View.VISIBLE);
            Double diff = 0.0;
            diff = total * (promo.get(0).getPercent() / 100);
            total = total - diff;
            promo_code.setText(promo.get(0).getCode() + " with "+ promo.get(0).getPercent().intValue() +"%");
            cart_total.setText(String.format("PHP %.2f", total));
        }
        Log.e("age", ""+getAge(user.getData().getBuyer().getBuyDoB())+"age"+user.getData().getBuyer().getBuyDoB());
        if (getAge(user.getData().getBuyer().getBuyDoB()) >= 60) {
            layout_pwd.setVisibility(View.VISIBLE);
            Double diff = 0.0;
            diff = total * 0.20;
            total = total - diff;
            cart_total_pwd_discount.setText("A PWD Discount with "+pwd_total.toString()+"%");
            cart_total.setText(String.format("PHP %.2f", total));
        }
        total = total + 100;

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.payment_type_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        delivery_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(PaymentAndShippingActvity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        delivery_time.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, false);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        delivery_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(v.getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomOrderMasterlistController customOrderMasterlistController = new CustomOrderMasterlistController(PaymentAndShippingActvity.this);
                try {
                    customOrderMasterlistController.start();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Session session = new Session(PaymentAndShippingActvity.this);
                OrderMasterlist.Datum datum = new OrderMasterlist.Datum();
                datum.setTotAmount(total);
                datum.setPaymentType(spinner.getSelectedItem().toString());
                datum.setDeliveryDate(delivery_date.getText().toString() + " " + delivery_time.getText().toString());
                datum.setShippingAdd(shipping_address.getText().toString());
                datum.setBUYERID(session.getId(v.getContext().getString(R.string.id)));
                customOrderMasterlistController.store(datum);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(PaymentAndShippingActvity.this, HomePageActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
        return true;
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        delivery_date.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void response(Object result, String message, Integer statusCode) {
        if(result instanceof CustomOrderMasterlist.Data) {
            CustomOrderMasterlist.Data customOrderMasterlist = (CustomOrderMasterlist.Data) result;
            OrderMasterlist.Datum datum1 = new OrderMasterlist.Datum();
            datum1.setTotAmount(Double.parseDouble(customOrderMasterlist.getTotAmount()));
            datum1.setPaymentType(customOrderMasterlist.getPaymentType());
            datum1.setDeliveryDate(customOrderMasterlist.getDeliveryDate());
            datum1.setShippingAdd(customOrderMasterlist.getShippingAdd());
            datum1.setOrMasID(customOrderMasterlist.getOrMasID());
            datum1.setOrDetID(customOrderMasterlist.getOrMasID());
            CustomOrderMasterlistController customOrderMasterlistController = new CustomOrderMasterlistController(PaymentAndShippingActvity.this);
            try {
                customOrderMasterlistController.start();
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            customOrderMasterlistController.update(datum1);
            OrderDetail.Datum datum = new OrderDetail().new Datum();
            for(Product.Datum datum2 : this.datum){
                datum.setOrDetID(datum1.getOrMasID());
                datum.setOrDetCount(datum2.getProdQuantity());
                datum.setOrDetItemDiscount(0);
                datum.setOrDetDate(datum1.getDeliveryDate());
                datum.setProdID(datum2.getProdID());
                customOrderMasterlistController.insertOrderDetail(datum);
                customOrderMasterlistController.subtractProduct(datum);
            }

            AlertDialog.Builder builder1 = new AlertDialog.Builder(PaymentAndShippingActvity.this);
            builder1.setMessage("Order is succesfull. Proceed to transaction tab for the Order status!");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Session session = new Session(PaymentAndShippingActvity.this);
                            session.clearSession(getBaseContext().getString(R.string.cart));
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();

            alert11.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Intent i = new Intent(PaymentAndShippingActvity.this, HomePageActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            });

        } else {
            Log.e("shipping", "OrderDetail"+result.toString());
        }
    }

    @Override
    public void fail(Throwable stackTrace) {
        System.out.println(stackTrace.getMessage());
    }

    private int getAge(String dobString){

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month+1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }



        return age;
    }
}
