package com.prototype.kompra.controller;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.adapter.OrderDetailProductsAdapter;
import com.prototype.kompra.api.ReturnApi;
import com.prototype.kompra.model.Return;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReturnController implements Callback<Return> {

    OrderDetailProductsAdapter.ProductViewHolder orderDetailProductsAdapter;
    Gson gson;
    Retrofit retrofit;
    Context context;

    public ReturnController(OrderDetailProductsAdapter.ProductViewHolder orderDetailProductsAdapter, Context context) {
        this.orderDetailProductsAdapter = orderDetailProductsAdapter;
        this.context = context;
    }

    public void start() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(context).build())
                .build();
    }

    @Override
    public void onResponse(Call<Return> call, Response<Return> response) {

        if(response.isSuccessful()) {
            Return aReturn = response.body();
            orderDetailProductsAdapter.response(aReturn.getData(), response.message(), aReturn.getStatusCode());
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<Return> call, Throwable t) {
        orderDetailProductsAdapter.fail(t);
    }

    public void addReturn(Return.Datum r) {
        ReturnApi returnApi = retrofit.create(ReturnApi.class);

        Call<Return> call = returnApi.addReturn(r);
        call.enqueue(this);
    }
}
