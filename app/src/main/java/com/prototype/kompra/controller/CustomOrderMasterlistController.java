package com.prototype.kompra.controller;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prototype.kompra.activity.ClaimActivity;
import com.prototype.kompra.activity.FeedbackActivity;
import com.prototype.kompra.activity.InProgressActivity;
import com.prototype.kompra.activity.PaymentAndShippingActvity;
import com.prototype.kompra.api.OrderDetailApi;
import com.prototype.kompra.api.OrderMasterlistApi;
import com.prototype.kompra.api.ProductApi;
import com.prototype.kompra.model.CustomOrderMasterlist;
import com.prototype.kompra.model.OrderDetail;
import com.prototype.kompra.model.OrderMasterlist;
import com.prototype.kompra.model.Product;
import com.prototype.kompra.util.App;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CustomOrderMasterlistController implements Callback<CustomOrderMasterlist> {

    private Gson gson;
    private Retrofit retrofit;
    private AppCompatActivity appCompatActivity;

    public CustomOrderMasterlistController(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    public void start() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {

        gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(App.getHttpClient(appCompatActivity.getApplicationContext()).build())
                .build();

    }

    @Override
    public void onResponse(Call<CustomOrderMasterlist> call, Response<CustomOrderMasterlist> response) {
        if(response.isSuccessful()) {
            CustomOrderMasterlist customOrderMasterlist = response.body();
            if(appCompatActivity instanceof PaymentAndShippingActvity) {
                ((PaymentAndShippingActvity) appCompatActivity).response(customOrderMasterlist.getData(), response.message(), customOrderMasterlist.getStatusCode());
            } else if (appCompatActivity instanceof ClaimActivity) {
                ((ClaimActivity) appCompatActivity).response(customOrderMasterlist.getData(), response.message(), customOrderMasterlist.getStatusCode());
            } else if (appCompatActivity instanceof InProgressActivity) {
                ((InProgressActivity) appCompatActivity).response(customOrderMasterlist.getData(), response.message(), customOrderMasterlist.getStatusCode());
            } else if (appCompatActivity instanceof FeedbackActivity) {
                ((FeedbackActivity) appCompatActivity).response(customOrderMasterlist.getData(), response.message(), customOrderMasterlist.getStatusCode());
            }
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<CustomOrderMasterlist> call, Throwable t) {
        if(appCompatActivity instanceof PaymentAndShippingActvity) {
            ((PaymentAndShippingActvity) appCompatActivity).fail(t);
        } else if (appCompatActivity instanceof ClaimActivity) {
            ((ClaimActivity) appCompatActivity).fail(t);
        } else if (appCompatActivity instanceof InProgressActivity) {
            ((InProgressActivity) appCompatActivity).fail(t);
        } else if (appCompatActivity instanceof FeedbackActivity) {
            Log.e("FeedbackActivity", ""+t.getMessage());
            ((FeedbackActivity) appCompatActivity).fail(t);
        }
    }

    public void store(OrderMasterlist.Datum datum) {

        OrderMasterlistApi orderMasterlistApi = retrofit.create(OrderMasterlistApi.class);

        Call<CustomOrderMasterlist> call = orderMasterlistApi.addOrderMasterlist(datum);
        call.enqueue(this);
    }

    public void update(OrderMasterlist.Datum datum) {

        OrderMasterlistApi orderMasterlistApi = retrofit.create(OrderMasterlistApi.class);

        Call<CustomOrderMasterlist> call = orderMasterlistApi.updateOrderMasterlist(datum.getOrMasID(), datum);
        call.enqueue(this);
    }

    public void insertOrderDetail(OrderDetail.Datum orderDetail) {

        OrderDetailApi orderDetailApi = retrofit.create(OrderDetailApi.class);
        Call<OrderDetail> call = orderDetailApi.addOrderDetail(orderDetail);
        call.enqueue(new Callback<OrderDetail>() {
            @Override
            public void onResponse(Call<OrderDetail> call, Response<OrderDetail> response) {
                if(response.isSuccessful()) {
                    OrderDetail datum= response.body();
                    ((PaymentAndShippingActvity) appCompatActivity).response(datum.getData(), response.message(), datum.getStatusCode());
                } else {
                    System.out.println(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<OrderDetail> call, Throwable t) {

                ((PaymentAndShippingActvity) appCompatActivity).fail(t);
            }
        });
    }

    public void subtractProduct(OrderDetail.Datum orderDetail) {

        ProductApi productApi = retrofit.create(ProductApi.class);
        Product.Datum datum = new Product().new Datum();
        datum.setProdQuantity(orderDetail.getOrDetCount());
        Call<Product> call = productApi.subtractProduct((int)orderDetail.getProdID(), datum);
        call.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                if(response.isSuccessful()) {
                    System.out.println(response.message());
                } else {
                    System.out.println(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }
}
